from django.urls import path

from .views import (
    api_users,
    api_user_token,
    api_get_current_user,
    api_user_detail_view,
    api_ratings,
)

urlpatterns = [
    path("users/", api_users, name="api_users"),
    path("users/me/token/", api_user_token, name="api_user_token"),
    path("users/me/", api_get_current_user, name="api_get_current_user"),
    path("users/<int:users_id>/", api_user_detail_view, name="api_user_detail_view"),
    path("users/ratings/", api_ratings, name="api_ratings"),
]
