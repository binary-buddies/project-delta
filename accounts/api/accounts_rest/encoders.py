from common.json import ModelEncoder
from .models import Rating, User


class UserEncoder(ModelEncoder):
    model = User
    properties = ["id", "username", "email"]


class RatingEncoder(ModelEncoder):
    model = Rating
    properties = ["seller", "buyer", "number"]
    encoders = {"seller": UserEncoder()}
