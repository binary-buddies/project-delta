import json
from django.db import IntegrityError
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import djwto.authentication as auth
from .encoder import UserEncoder, RatingEncoder
from .models import User, Rating


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_users(request):
    if request.method == "GET":
        user = User.objects.all()
        return JsonResponse({"users": user}, encoder=UserEncoder, safe=False)
    else:
        try:
            content = json.loads(request.body)
            user = User.objects.create_user(**content)
            return JsonResponse({"id": user.id, "username": user.username})
        except IntegrityError:
            response = JsonResponse(
                {"detail": "Please enter a different username and email"}
            )
            response.status_code = 409
            return response


# get individual user details
@require_http_methods(["GET", "PUT"])
def api_user_detail_view(request, users_id):
    if request.method == "GET":
        user = User.objects.get(id=users_id)
        return JsonResponse({"users": user}, encoder=UserEncoder, safe=False)
    else:  # PUT
        try:
            content = json.loads(request.body)
            user = User.objects.get(id=users_id)
            props = ["zip", "city", "state", "is_staff", "is_superuser"]
            for prop in props:
                print(prop)
                if prop in content:
                    setattr(user, prop, content[prop])
                user.save()
            return JsonResponse(
                user,
                encoder=UserEncoder,
                safe=False,
            )
        except User.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def api_user_token(request):
    if "jwt_access_token" in request.COOKIES:
        token = request.COOKIES["jwt_access_token"]
        if token:
            return JsonResponse(
                {
                    "token": token,
                },
            )
    response = JsonResponse({"detail": "no session"})
    response.status_code = 404
    return response


@require_http_methods(["GET"])
@auth.jwt_login_required
def api_get_current_user(request):
    print(request.payload)
    user = User.objects.get(id=request.payload["user"]["id"])
    return JsonResponse(
        {
            "id": user.id,
            "username": user.username,
            "email": user.email,
            "is_staff": user.is_staff,
            "city": user.city,
            "state": user.state,
            "zip": user.zip,
        }
    )


@require_http_methods(["GET", "PUT"])
def api_ratings(request):
    if request.method == "GET":
        ratings = Rating.objects.all()
        return JsonResponse({"ratings": ratings}, encoder=RatingEncoder, safe=False)
    else:
        try:
            content = json.loads(request.body)
            rating = Rating.objects.create(**content)
            return JsonResponse({"rating": rating}, encoder=RatingEncoder, safe=False)
        except Exception:
            return JsonResponse({"message": "could not create rating"})


# 'user': {'username': 'jessicadyer', 'id': 1, 'perms': []}
