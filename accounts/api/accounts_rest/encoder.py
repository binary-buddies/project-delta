from common.json import ModelEncoder
from .models import User, Rating


class UserEncoder(ModelEncoder):
    model = User
    properties = ["id", "email", "city", "state", "zip", "is_staff", "is_superuser"]


class RatingEncoder(ModelEncoder):
    model = Rating
    properties = ["seller", "buyer", "number"]
    encoders = {"seller": UserEncoder()}
