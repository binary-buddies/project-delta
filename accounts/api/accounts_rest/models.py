from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class User(AbstractUser):
    email = models.EmailField(unique=True)
    city = models.CharField(max_length=100, null=True)
    latitude = models.DecimalField(
        blank=True, decimal_places=16, max_digits=22, null=True
    )
    longitude = models.DecimalField(
        blank=True, decimal_places=16, max_digits=22, null=True
    )
    state = models.CharField(max_length=50, null=True)
    zip = models.CharField(max_length=5, null=True)


class Rating(models.Model):
    seller = models.ForeignKey(User, related_name="user", on_delete=models.PROTECT)
    buyer = models.EmailField(max_length=254)
    number = models.SmallIntegerField(
        default=5, validators=[MaxValueValidator(5), MinValueValidator(1)]
    )
