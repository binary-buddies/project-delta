from rest_framework import serializers
from .models import Item

# this file is needed to handle the serializers to upload an image with FormData


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = "__all__"
