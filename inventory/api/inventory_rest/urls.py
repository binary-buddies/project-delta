from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from .views import (
    api_categories,
    api_manufacturers,
    api_items,
    api_item_accept_offer,
    api_item_unaccept_offer,
    api_items_for_sale,
    api_items_submitted,
    api_items_for_sale_by_user,
    api_items_pending_sales_by_user,
    api_items_sold_by_user,
    api_item_mark_item_sold,
    api_item,
    api_activities,
    api_queries,
    api_query,
    api_approve_item,
    api_reject_item,
    api_queries_user,
    api_item_beta,
    api_activity,
    api_category,
    api_manufacturer
)

urlpatterns = [
    path("categories/<int:pk>/", api_category, name="api_category"),
    path("activities/<int:pk>/", api_activity, name="api_activity"),
    path("manufacturers/<int:pk>/", api_manufacturer, name="api_manufacturer"),
    path("categories/", api_categories, name="api_categories"),
    path("manufacturers/", api_manufacturers, name="api_manufacturers"),
    path("items/", api_items, name="api_items"),
    path("items/<int:item_id>/", api_item, name="api_item"),
    path("item_beta/<int:pk>/", api_item_beta, name="api_item_beta"),
    path("items/available/", api_items_for_sale, name="api_items_available"),
    path("items/submitted/", api_items_submitted, name="api_items_submitted"),
    path(
        "items/available/<str:user_email>/",
        api_items_for_sale_by_user,
        name="api_items_available_by_user",
    ),
    path(
        "items/pending_sales/<str:user_email>/",
        api_items_pending_sales_by_user,
        name="api_items_pending_sales_by_user",
    ),
    path(
        "items/mark_sold/<int:item_id>/",
        api_item_mark_item_sold,
        name="api_item_mark_item_sold",
    ),
    path(
        "items/sold/<str:user_email>/",
        api_items_sold_by_user,
        name="api_items_sold_by_user",
    ),
    path(
        "items/<int:item_id>/accept_offer/",
        api_item_accept_offer,
        name="api_accept_offer",
    ),
    path(
        "items/<int:item_id>/unaccept_offer/",
        api_item_unaccept_offer,
        name="api_unaccept_offer",
    ),
    path("activities/", api_activities, name="api_activities"),
    path("queries/", api_queries, name="api_queries"),
    path("query/<int:query_id>/", api_query, name="api_query"),
    path("queries/<str:user_email>/", api_queries_user, name="api_queries_user"),
    path("item/<int:pk>/approved/", api_approve_item, name="api_approve_item"),
    path("item/<int:pk>/rejected/", api_reject_item, name="api_reject_item"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
