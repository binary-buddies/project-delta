from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from django.db.models import Sum
import pika
import os

from .encoders import (
    CategoryEncoder,
    ManufacturerEncoder,
    ItemListEncoder,
    ActivityEncoder,
    QueryEncoder,
)

from .models import Activity, Category, Manufacturer, Item, Query

# These 4 imports below are needed for handling the Multipart FormData for image uploads in Item Post
from .serializers import ItemSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status


# CATEGORIES
@require_http_methods(["GET", "POST"])
def api_categories(request):
    if request.method == "GET":
        categories = Category.objects.all()
        return JsonResponse({"categories": categories}, encoder=CategoryEncoder)
    else:
        try:
            content = json.loads(request.body)
            category = Category.objects.create(**content)
            return JsonResponse(
                category,
                encoder=CategoryEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse({"message": "Could not create the category"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_category(request, pk):
    if request.method == "DELETE":
        try:
            category = Category.objects.get(id=pk)
            category.delete()
            response = JsonResponse({"deleted": True})
            return response
        except Category.DoesNotExist:
            return JsonResponse("That is not a valid category")


# ACTIVITIES
@require_http_methods(["GET", "POST"])
def api_activities(request):
    if request.method == "GET":
        activities = Activity.objects.all()
        return JsonResponse({"activities": activities}, encoder=ActivityEncoder)
    else:
        try:
            content = json.loads(request.body)
            activity = Activity.objects.create(**content)
            return JsonResponse(
                activity,
                encoder=ActivityEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse({"message": "Could not create the activity"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_activity(request, pk):
    if request.method == "DELETE":
        try:
            activity = Activity.objects.get(id=pk)
            activity.delete()
            response = JsonResponse({"deleted": True})
            return response
        except Category.DoesNotExist:
            return JsonResponse("That is not a valid activity")


# MANUFACTURERS
@require_http_methods(["GET", "POST"])
def api_manufacturers(request):
    if request.method == "GET":
        brands = Manufacturer.objects.all()
        return JsonResponse({"brands": brands}, encoder=ManufacturerEncoder)
    else:
        try:
            content = json.loads(request.body)
            brand = Manufacturer.objects.create(**content)
            return JsonResponse(
                brand,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse({"message": "Could not create the brand"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_manufacturer(request, pk):
    if request.method == "DELETE":
        try:
            manufacturer = Manufacturer.objects.get(id=pk)
            manufacturer.delete()
            response = JsonResponse({"deleted": True})
            return response
        except Category.DoesNotExist:
            return JsonResponse("That is not a valid manufacturer")


# ITEMS
# Changed this from require http to api_view to handle new request datatype for uploading an image.
# Couldn't use just the JSON, needed the serializers
@api_view(["GET", "POST"])
def api_items(request):
    if request.method == "GET":
        item = Item.objects.all()
        return JsonResponse({"item": item}, encoder=ItemListEncoder)
    else:
        try:
            items_serializer = ItemSerializer(data=request.data)
            if items_serializer.is_valid():
                items_serializer.save()
                return Response(items_serializer.data, status=status.HTTP_201_CREATED)
            else:
                print("error", items_serializer.errors)
                return Response(
                    items_serializer.errors, status=status.HTTP_400_BAD_REQUEST
                )

        except Exception as e:
            print(e)
            response = JsonResponse({"message": "Could not create the item of gear"})
            response.status_code = 400
            return response


@api_view(["GET", "PUT"])
def api_item_beta(request, pk):
    try:
        item = Item.objects.get(pk=pk)
    except Item.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == "GET":
        serializer = ItemSerializer(item)
        return Response(serializer.data)
    elif request.method == "PUT":
        serializer = ItemSerializer(item, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# GET ITEM OR EDIT ITEM
@require_http_methods(["GET", "PUT", "DELETE"])
def api_item(request, item_id):
    if request.method == "GET":
        try:
            item = Item.objects.get(id=item_id)
            return JsonResponse(item, encoder=ItemListEncoder, safe=False)
        except Item.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "PUT":
        try:
            data = json.loads(request.body)
            item = Item.objects.get(id=item_id)

            props = [
                "title",
                "condition",
                "category",
                "activity",
                "manufacturer",
                "color",
                "size",
                "description",
                "msrp",
                "price",
            ]

            for prop in props:
                if prop in data:
                    setattr(item, prop, data[prop])
            item.save()
            return JsonResponse(item, encoder=ItemListEncoder, safe=False)
        except Item.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Item.objects.get(id=item_id).delete()
            return JsonResponse({"deleted": count > 0})
        except Item.DoesNotExist:
            return JsonResponse({"message": "Item does not exist."})


# Retrieves all items that are not marked as sold that have been approved by admin
@require_http_methods(["GET"])
def api_items_for_sale(request):
    try:
        available_items = Item.objects.filter(
            sold=False, status="Approved", accepted_offer=None
        )
        return JsonResponse(
            {"items": available_items},
            encoder=ItemListEncoder,
        )
    except Exception:
        response = JsonResponse({"message": "Could not retrieve items"})
        response.status_code = 400
        return response


# Retrieves all items with a "submitted" status
# This is for the admin page to approve or reject items
@require_http_methods(["GET"])
def api_items_submitted(request):
    try:
        submitted_items = Item.objects.filter(status="Submitted")
        return JsonResponse(
            {"items": submitted_items},
            encoder=ItemListEncoder,
        )
    except Exception:
        response = JsonResponse({"message": "Could not retrieve items"})
        response.status_code = 400
        return response


# Retrieves items for sale by specific user that are approved, not sold, with no offers
@require_http_methods(["GET"])
def api_items_for_sale_by_user(request, user_email):
    try:
        available_items = Item.objects.filter(
            seller=user_email,
            sold=False,
            accepted_offer__isnull=True,
            status="Approved",
        )
        return JsonResponse(
            {"items": available_items},
            encoder=ItemListEncoder,
        )
    except Exception:
        response = JsonResponse({"message": "Could not retrieve items"})
        response.status_code = 400
        return response


# Retrieves items for sale by specific user that are approved,
# not sold, with an accepted offer
@require_http_methods(["GET"])
def api_items_pending_sales_by_user(request, user_email):
    try:
        available_items = Item.objects.filter(
            seller=user_email,
            sold=False,
            accepted_offer__isnull=False,
            status="Approved",
        )
        return JsonResponse(
            {"items": available_items},
            encoder=ItemListEncoder,
        )
    except Exception:
        response = JsonResponse({"message": "Could not retrieve items"})
        response.status_code = 400
        return response


# Retrieves items that have been sold by a specific user
@require_http_methods(["GET"])
def api_items_sold_by_user(request, user_email):
    try:
        available_items = Item.objects.filter(seller=user_email, sold=True)
        price_sum = Item.objects.filter(seller=user_email, sold=True).aggregate(
            Sum("price")
        )
        revenue = 0
        if price_sum["price__sum"] is not None:
            revenue = price_sum["price__sum"]
        return JsonResponse(
            {"revenue": revenue, "items": available_items},
            encoder=ItemListEncoder,
        )
    except Exception:
        response = JsonResponse({"message": "Could not retrieve items"})
        response.status_code = 400
        return response


@require_http_methods(["PUT"])
def api_item_accept_offer(request, item_id):
    try:
        content = json.loads(request.body)
        item = Item.objects.get(id=item_id)
        props = ["accepted_offer"]
        for prop in props:
            if prop in content:
                setattr(item, prop, content[prop])
        item.save()

        if "buyer_email" in content:
            accepted_offer_dictionary = {
                "seller_email": item.seller,
                "buyer_email": content["buyer_email"],
                "item_title": item.title,
            }
            print(accepted_offer_dictionary)
            send_message(
                json.dumps(accepted_offer_dictionary), "process_accepted_offer"
            )
            print("message sent to process accepted offer")

        return JsonResponse(
            item,
            encoder=ItemListEncoder,
            safe=False,
        )
    except Item.DoesNotExist:
        response = JsonResponse({"message": "Item does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_item_unaccept_offer(request, item_id):
    try:
        item = Item.objects.get(id=item_id)
        setattr(item, "accepted_offer", None)
        item.save()
        return JsonResponse(
            item,
            encoder=ItemListEncoder,
            safe=False,
        )
    except Item.DoesNotExist:
        response = JsonResponse({"message": "Item does not exist"})
        response.status_code = 404
        return response


# Mark item as sold
@require_http_methods(["PUT"])
def api_item_mark_item_sold(request, item_id):
    try:
        item = Item.objects.get(id=item_id)
        setattr(item, "sold", True)
        item.save()
        return JsonResponse(
            item,
            encoder=ItemListEncoder,
            safe=False,
        )
    except Item.DoesNotExist:
        response = JsonResponse({"message": "Item does not exist"})
        response.status_code = 404
        return response


# QUERIES
@require_http_methods(["GET", "POST"])
def api_queries(request):
    if request.method == "GET":
        queries = Query.objects.all()
        return JsonResponse({"queries": queries}, encoder=QueryEncoder)
    else:
        try:
            content = json.loads(request.body)
            query = Query.objects.create(**content)
            return JsonResponse(
                query,
                encoder=QueryEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse({"message": "Could not create the query"})
            response.status_code = 400
            return response


@require_http_methods(["GET"])
def api_queries_user(request, user_email):
    if request.method == "GET":
        queries = Query.objects.filter(user=user_email)
        count = Query.objects.filter(user=user_email).count()
        return JsonResponse({"count": count, "queries": queries}, encoder=QueryEncoder)


@require_http_methods(["DELETE", "GET"])
def api_query(request, query_id):
    if request.method == "GET":
        query = Query.objects.get(id=query_id)
        return JsonResponse(query, encoder=QueryEncoder, safe=False)
    else:
        count, _ = Query.objects.filter(id=query_id).delete()
        return JsonResponse({"deleted": count > 0})


def send_message(data, queueName):
    parameters = pika.URLParameters(os.environ["CLOUDAMQP_URL"])
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queueName)
    channel.basic_publish(
        exchange="",
        routing_key=queueName,
        body=data,
    )
    connection.close()
    return


def all_query_words_match(title: list[str], query: list[str]) -> bool:
    title = title.lower().split()
    query = query.lower().split()
    matches = 0
    for word in query:
        if word in title:
            matches += 1
    if matches == len(query):
        return True
    return False


@require_http_methods(["PUT"])
def api_approve_item(request, pk):
    item = Item.objects.get(id=pk)
    item.approve()
    item_dictionary = {
        "seller_email": item.seller,
        "item_title": item.title,
        "item_id": item.id,
        "status": item.status,
    }
    send_message(json.dumps(item_dictionary), "process_approval")

    queries = Query.objects.all()
    matching_queries = []
    for query in queries:
        if all_query_words_match(item.title, query.query):
            matching_dict = {
                "user_email": query.user,
                "query": query.query,
                "matching_item": item.title,
                "item_id": item.id,
            }
            if matching_dict not in matching_queries:
                matching_queries.append(matching_dict)

    for matching_query in matching_queries:
        send_message(json.dumps(matching_query), "process_query_match")

    return JsonResponse(item, encoder=ItemListEncoder, safe=False)


@require_http_methods(["PUT"])
def api_reject_item(request, pk):
    item = Item.objects.get(id=pk)
    item.reject()
    item_dictionary = {
        "seller_email": item.seller,
        "item_title": item.title,
        "item_id": item.id,
        "status": item.status,
    }
    print(item_dictionary)
    send_message(json.dumps(item_dictionary), "process_rejection")
    return JsonResponse(item, encoder=ItemListEncoder, safe=False)
