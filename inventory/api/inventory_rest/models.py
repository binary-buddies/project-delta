from django.db import models
from django.urls import reverse


class Query(models.Model):
    user = models.EmailField()
    query = models.CharField(max_length=100)

    def __str__(self):
        return self.query

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "queries"


class Activity(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self) -> str:
        return self.name

    class Meta:
        ordering = ["name"]


class Category(models.Model):

    name = models.CharField(max_length=100, unique=True)

    class Meta:
        ordering = ["name"]

    def get_api_url(self):
        return reverse("api_category", kwargs={"pk": self.id})

    def __str__(self) -> str:
        return self.name


class Manufacturer(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_manufacturer", kwargs={"pk": self.id})

    def __str__(self) -> str:
        return self.name

    class Meta:
        ordering = ["name"]


class Item(models.Model):
    class ConditionChoices(models.TextChoices):
        NEW = "New"
        RECONDITIONED = "Reconditioned"
        OPEN_BOX = "Open box (never used)"
        USED = "Used (normal wear)"
        FOR_PARTS = "For parts"
        OTHER = "Other (see description)"

    class StatusChoices(models.TextChoices):
        SUBMITTED = "Submitted"
        APPROVED = "Approved"
        REJECTED = "Rejected"

    seller = models.EmailField()
    title = models.CharField(max_length=250)
    city = models.CharField(max_length=100, null=True)
    state = models.CharField(max_length=100, null=True)
    image = models.ImageField(upload_to="images/", blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True)
    category = models.ForeignKey(
        Category, related_name="item", on_delete=models.PROTECT, null=True
    )
    activity = models.ForeignKey(
        Activity, related_name="item", on_delete=models.PROTECT, null=True
    )
    condition = models.CharField(
        max_length=25, choices=ConditionChoices.choices, default=ConditionChoices.USED
    )
    manufacturer = models.ForeignKey(
        Manufacturer, related_name="item", on_delete=models.PROTECT
    )
    color = models.CharField(max_length=50)
    size = models.CharField(max_length=50)
    msrp = models.DecimalField(max_digits=9, decimal_places=2)
    description = models.TextField(null=True)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    sold = models.BooleanField(default=False)
    accepted_offer = models.IntegerField(blank=True, null=True)
    status = models.CharField(
        max_length=10, choices=StatusChoices.choices, default=StatusChoices.SUBMITTED
    )

    def get_api_url(self):
        return reverse("api_item", kwargs={"pk": self.id})

    def __str__(self) -> str:
        return self.title

    def approve(self):
        self.status = "Approved"
        self.save()

    def reject(self):
        self.status = "Rejected"
        self.save()

    class Meta:
        ordering = ["created_at"]
