from common.json import ModelEncoder

from .models import Category, Manufacturer, Item, Activity, Query


class QueryEncoder(ModelEncoder):
    model = Query
    properties = ["id", "user", "query"]


class CategoryEncoder(ModelEncoder):
    model = Category
    properties = ["id", "name"]


class ActivityEncoder(ModelEncoder):
    model = Activity
    properties = ["id", "name"]


class ManufacturerEncoder(ModelEncoder):
    model = Manufacturer
    properties = ["id", "name"]


class ItemListEncoder(ModelEncoder):
    model = Item
    properties = [
        "id",
        "seller",
        "title",
        "city",
        "state",
        "created_at",
        "category",
        "activity",
        "condition",
        "manufacturer",
        "color",
        "size",
        "msrp",
        "description",
        "price",
        "sold",
        "accepted_offer",
        "status",
    ]
    encoders = {
        "activity": ActivityEncoder(),
        "category": CategoryEncoder(),
        "manufacturer": ManufacturerEncoder(),
    }

    def get_extra_data(self, o):
        try:
            return {"image": o.image.url}
        except Exception:
            return {"image": ""}
