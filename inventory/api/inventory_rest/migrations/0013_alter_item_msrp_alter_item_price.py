# Generated by Django 4.0.3 on 2022-10-01 18:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory_rest', '0012_item_city_item_state'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='msrp',
            field=models.DecimalField(decimal_places=2, max_digits=9),
        ),
        migrations.AlterField(
            model_name='item',
            name='price',
            field=models.DecimalField(decimal_places=2, max_digits=9),
        ),
    ]
