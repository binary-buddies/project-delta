# Generated by Django 4.0.3 on 2022-06-06 23:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("inventory_rest", "0010_item_status"),
    ]

    operations = [
        migrations.AlterField(
            model_name="item",
            name="status",
            field=models.CharField(
                choices=[
                    ("Submitted", "Submitted"),
                    ("Approved", "Approved"),
                    ("Rejected", "Rejected"),
                ],
                default="Submitted",
                max_length=10,
            ),
        ),
    ]
