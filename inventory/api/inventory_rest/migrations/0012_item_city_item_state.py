# Generated by Django 4.0.3 on 2022-06-14 03:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("inventory_rest", "0011_alter_item_status"),
    ]

    operations = [
        migrations.AddField(
            model_name="item",
            name="city",
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name="item",
            name="state",
            field=models.CharField(max_length=100, null=True),
        ),
    ]
