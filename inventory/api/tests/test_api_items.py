from django.test import TestCase, Client
import json


class ApiItemsTests(TestCase):
    @classmethod
    def setUp(self):
        self.client = Client()

    def test_get_items(self):
        response = self.client.get("/api/items/")
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn("item", content)
