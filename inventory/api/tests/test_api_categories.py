from django.test import TestCase, Client
import json


class ApiCategoryTests(TestCase):
    @classmethod
    def setUp(self):
        self.client = Client()

    def test_get_categories(self):
        response = self.client.get("/api/categories/")
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn("categories", content)

    def test_post_category(self):
        category = json.dumps({"name": "testing"})
        response = self.client.post("/api/categories/", category, "application/json")
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn("name", content)
