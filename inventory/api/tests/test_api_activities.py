from django.test import TestCase, Client
import json


class ApiActivityTests(TestCase):
    @classmethod
    def setUp(self):
        self.client = Client()

    def test_get_activities(self):
        response = self.client.get("/api/activities/")
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn("activities", content)

    def test_post_activity(self):
        activity = json.dumps({"name": "testing"})
        response = self.client.post("/api/activities/", activity, "application/json")
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn("name", content)
