Matty's Journal
## June 24, 2022
- Deployment continues. All worked really hard to get deployed. Ran into a dead end with a keys file and CORS errors
- But the site is visible on Heroku.  Awaiting assistance to help resolve pending issues with ACLS keys fails, how to address mail and rabbit mq.
- Continued efforts to deploy. Noted unable to log out, access admin, see images, add sale details.
- We were able to access admin after changing end point to access admin and make super users and staff.
- Able to add and delete details resolved on site
- Still having 404 issues  with images, mailer not working, weather/google API no working, unable to log out of app
## June 23, 2022
- Deployment day was a very busy day of running Black and Flake8 over all files and correcting issues. 
- CI to GIT integration took about 5 hours to resolve all front end issues
- We ended up not using npm i scroll-to-top as the dependencies kept causes issues (We were all sad about that)
- The back end went a little quicker, mainly removing line blocks and unused imports
- Then on to Heroku, adding variables, keys, values and updating the .gitlab_ci.yml file for release and deployment
## June 22, 2022
- Reviewed goals with team on for week, plan to code freeze later today
- Testing new code from pull, noticed that Admin did not have ability to add activity
- Updated Nav, and New Activities.js for include same and updated nav
- Noticed  that search bar is carrying state to other pages and filter after page change
- Help ticket for same to better understand how state is carried and when it rests
-
- Updated code to show card properly after searching using search bar
- Reviewed UI experience and noticed that when detail page loads it loads to same location on page
- IE if at boot of the main page and click an item for details, it will load the detail page at the bottom
- Looked into same, updated code with 2 items: npm ScrollToTop and add a component for the same ScrollToTop
- npm ScrollToTop adds a button to all gear page to jump back to top of page
- ScrollToTop component will take a user to the top of the detail page when it loads
- Discussed with group the idea of the Admin being notified when an item needs to be auth'd or denied before posting
- Agreed that can be addressed after deployment in future growth of project
## June 21, 2022
- Morning group stand up to go over what features are still pending for MVP for end of week
- Discussed what features we want to search bar, edit items, edit user info, user dashboard,  url clean-up
- GoogleMapsEmbed hide keys
- Discussed with Curtis the future of Isella, round tabled rest with team via slack
- Focussed on search bar in nav. Made a new page to address search results. 
- Worked with Mitch on using setState and using the Nav bar to carry state at a higher level.
- Add .includes to filter to help in search and using an open string
- Noticed that pages when searching by drop down that will not reset after a search
- Added and or statement to pages to reset back to "sort by manufacturer"
- 
## June 17, 2022
- Refactored GoogleMaps with Google Embed Maps. Why? Unlimited hits. 
- Set up new google project, new keys.
- Reviewed google Docs on parameters needed. Using IFrames, which was an npm import
- Why Iframe: google requires embed maps to be used in an IFrame
- Changed Weather API request because Google Embed map does not provide json information to confirm
## June 16, 2022
- Reviewed Google Maps API calls, looked into embed-ed maps vs google maps geocoding. Google api hits were a few thousand a day
- did some research on same and will look further into changing set up to embed-ed maps vs geocoding maps for later use
- reviewed with Jeff and limited API calls.. had to adjust componentDidUpdate so it would not make as many calls per page load.
- Will call once per page load vs thousand times
- Unit testing: created test for Items API confirmed 200 response
- Attempted to create test for POST method. Ran into issues with the json. Why? it was returning all info which included Primary Keys for categories, activity, manufacture. Those are Foreign Keys to other models. Reviewed with Chris/Shawn unable to get proper response.
## June 15, 2022
- Worked with Amanda V: team effort to get details page to work from all category pages
- Worked with Curtis on hiding API keys for Weather and GoogleApi. Noted that Google API
- Did not need to be hidden. Also noted that with GoogleMaps " <LoadScript/> " we were unable to put
- Keys in back-end. Resolved all and cleaned up some coding
- Set game plan for moving forward: want to get up some init test
- Worked with Amanda V after she did a pull, prior code had been uploaded and worked to resolve same
- Discussed importance of clean code and merge request and proper handling of same
## June 14, 2022
- GoogleMaps and Weather added to detail page and Weather to dashboard
- Based on user and/or Seller location
- Code was pushed and reviewed and merged to main
- Issues with pull from main, incomplete code. 
-  Worked with Amanda V and Jeff to resolve
- Ended up having to pull main and redo some coding to get back to before
## June 13, 2022
- Began working on linking up Weather/GoogleMap for user Dashboard
- User Dashboard will show current weather in user location. Pending completion
- Item for sale will show map of where item is. Pending completion
## June 10, 2022
- Group stand up on end of week goals
- Continued to work on Admin page
- Added is_staff and is_superuser to UserEndcoder
- Updated ability to add admin. Now admin can see all listed admins, search by email for a user.
- Also removed ability to changes is_superuser status: to protect from deletion 
- Admin link will hide from navbar if user is not admin auth'd
## June 9, 2022
- Stand up with team on current status and goals for end of week
- Continued on Admin page. Changed from one main page to 3 pages(add brand, ad category, pending post)
- Pending post review page for admin to Approve/Reject items ready for post. API calls to Items Submitted
- Additional pages for viewing all Approved and Rejected Items, ability to change status of items from Approved/Rejected
- Reviewed with Jessica on API end points and layout of pending post page on what info is needed
- Discussed with team on how to use User location for API google/Weather
- Still need to hide API keys in backend
- Working on how to restrict Admin access
.
- Worked and reviewed with Curtis Admin access and how to create a super user and created a plan added admins
## June 8, 2022
- Reviewed and updated List all Brands page
- Reviewed with team on a Git pull, pull ended up pulling a main with issues
- Spent time w Amanda and Antonio working out same
- Began work on Admin panel and links to same
- Will need ability to add brands, categories and to approve and deny post
- Discussed w Amanda V on what info we might need from user aka city state zip for listing
- Completed admin page for brands and categories, tomorrow plan is for approve/deny post

## June 7, 2022
- Noticed that copy of project that was made yesterday was not an up to date copy and lost yesterdays work
- Plan is to recreate filter and all pages with cards
- Climbing, Hiking, Ski, Clothing, List All rebuilt
- Reviewed with Amanda V on what information from the user is needed for location and listing items for sale
- Discussed options of using city, state vs zip code
- Reviewed Google Api docs and confirmed we can obtain location based on zip, will also supply city and state with
- Same search via zip
- Reviewed List All Manufactures and opted for a table vs a list of them. Had to take list of all manufactures and break into 
- Sub list of 4 each. Applied css to same for text-alignment
- Reviewed and researched React search box

## June 6, 2022
- Reviewed files updated by Curtis and group standup on plan for week
- Agreed to try to have functioning site ny end of week
- Was able to merge with main, yet late in morning had issues with pull from that branch
- Reviewed Help Ticket with Mitch, agreed to prune local side of project and re-clone
- Completed same and at end of day noted that the clone was from personal branch not from binary buddies branch
- Reviewed with team and agreed to prune local again and the re -clone from binary buddies branch again and the update new
- clone with today work. Updated new clone with current days work
- Current day: created a filter for all Items page to sort by manufacturer, added content to Climbing, Hiking/Camping, and Ski pages
- Utilized a component for ItemColumns and linked it to all pages, ability to filter by manufacturer on all pages
- Discussed ww team on usage of Brands page

- 
## June3, 2022
- Created endpoints to call in index.js and corresponding routes and paths in App.js
- Confirmed all link go to correct pages and placed initial React functions to hold places
- Worked w Amanda V on trying to figure out why images would nto load into folders
- Par programmed All Items list with card layout for inventory
- Git merger request and confirmed the create inventory page worked in React

# June 2, 2022
- React front end work and main page layout. Reviewed with group on layout and made adjustments
- Search bar added (currently not functional)
- Established links to pages
- Hero image added w text
- Bootstrap cards added as links as wel
- Worked with team on API for GoogleMaps

## June 1, 2022
- Group stand up and review fo project to date and what feature to focus on
- OpenWeather API set up with API key in .env
- OpenWeather component created and added to MapWeather folder, updated file paths
- Reviewed with group on style and location for the time
- discussed next step, will stay focused on React and follow Wireframes  
- Navbar added
## May 31, 2022
 - Created Accounts Project/App microservice into main branch
 - Google Maps geocode api call to Google to obtain longitude and latitude 
 - Integrated longitude and latitude into Google Maps API call
 - Will integrate with Accounts once ready for front end set up (City and State of the user)
 - Reviewed with team and pushed to main branch

 ## May 27-30, 2022
  - Researched maps API options for showing an items location via a google map
  - Created Google API key to obtain a map and geocode information for City and State
  - Reviewed options of using Google Maps vs OpenWeatherMaps and what React tools are available
  - Opted to use GoogleMaps integration to show the Google map via React
  - Added GoogleMap to React as a component to be used on any page as needed
  - created .env and added api key, updated .gitignore to not show   key
  
## May 26, 2022
 - Reviewed team projects and Asana to help sort what task need to be accomplished and who will be handling each
 - Discussed with group FexEd options and API calls to their server to obtain shipping rates
 - Noted that since shipping is an option for a seller/buyer and the choices that are available.
 - Opting at this point to set as a new stretch goal and/or a link on the seller's listing page to Fedex or USPS
 - Then a seller can opt for shipping and include rates in selling price as needed
 - Noted that USPS API request is written in XML
