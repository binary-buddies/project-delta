# Jessica's Journal

## June 22, 2022
- More code cleanup
- Cleaning up details/loose ends around the project.
- Added Dockerfiles to code as well as checked the requirements.txt


## June 21, 2022
- Changed all instances of 'localhost' to the updated variable names in features that we will be using for the demo and for MVP.
- Run Black on all python code
- Run Flake on all code as well.
- Added an email send when a seller receives an offer
- Calculated amount of money made by the user using the price they listed the item. Would be better if this was the actual cost it sold for. Will have to set up a poller to get the data from sales into inventory to do that.

## June 19, 2022
- It is unfortunate that we set this up with offers in one microservice and items in another microservice. Because the two are so tightly linked, it just doesn't make sense to have it set up like this. I'm having to come up with lots of solutions to update both at the same time. Would be nice to refactor this so that offers are inside inventory.
- Set up a poller to shuttle item information into Sales so when I'm displaying offers to users, I can show the item title, asking price and an image.
- Finished a draft of the "my purchases" page on the user dashboard. This retrieves all the offers in different states of the sale process and displays them to the viewer.
- Did more than is explained here. LOL. 5 hours of work today?

## June 18, 2022
- Set up the ability for a user to accept and unaccept an offer. The unaccept is in case the deal falls through. They can unaccept and the item will be re-listed in available inventory and other people can make offers on the item.
- Set up an email that connects buyer and seller after an offer has been accepted. Users can coordinate the sale from there.
- Spent an embarrassingly long time on frontend design stuff today. How long do you think it took me to figure out that the div tags on my modal were ruining the nice formatting in rows and columns? Once I figured out that the modal shouldn't have a div tag, I went a lot faster at designing the dashboard.
- Set up functionality to hide "sales" and show "purchases" on button click for dashboard navigation.
- 13 hours of working on this today.

## June 17, 2022
- I did some stuff this day but I can't remember exactly.

## June 16, 2022
- Created four new api endpoints to accommodate the process an item goes through when selling
- Integrated those endpoints into the frontend on the user dashboard

## June 15, 2022
- Continuing to refactor the user dashboard to match the MVP goals that I came up with last Friday.

## June 12, 2022
- Updated journal by taking myself on a git commit journey
- Worked to move my notes from a napkin taken at dinner on Friday into a cohesive path towards MVP for the team.
- Drew sales and offers flow in excalidraw for execution by the team
  - [Drawing here](../docs/Sales-offers-isella-2022-04-19-1157.png)
- Started to work on refactoring the user dashboard which will look slightly different with the new information
- Planned out how to execute the demo:
  - Will screen record all the features that we want to demo;
  - Note time in recording each feature starts
  - Assign team member to discuss each feature while other team member is running the recording

# Catching up on journal from May 31, 2022-June 10, 2022
  ## June 10, 2022
   - Got an alert to post and delete while passing a user to the Large Card successfully.
   - Started working on update item form
   - Did several hours of thought work to map out the path of a sale as well as the path of an offer.
     - [Front of drawing](../docs/Isella-MVP-notes-20220610_1.jpg)
     - [Back of drawing](../docs/Isella-MVP-notes-20220610_2.jpg)

  ## June 9, 2022
  - Worked with Matty to write an API endpoint that returns items that have been submitted but not yet approved
  - Stubbed out some tests for Matty to complete at a later date
  - Added a modal to delete active alerts, again passing around data that's loaded asynchronously. Async data is HARD.
  - Got a popover for the alerts section to work to show what an alert is to users

  ## June 8, 2022
  - Started working on an offers section of the dashboard to display current offers to the user
  - Worked a lot on passing around data coming from asynchronous functions
  - Somehow worked on the dashboard very late into the night

  ## June 7, 2022
  - Started on front end development. Specifically focused on the user dashboard, which will be the hub for user sales data and offers data.
  - Worked with Curtis to get a current user's information loaded upon login and scoped so that we can pass that info into components on App.js.
  - On dashboard, I was able to get the items a user is currently selling to display

  ## June 6, 2022
  - Added a query model to the inventory App to allow users to type in a key word or phrase to receive updates via email when an item with those words in the title are posted to the site.
  - Added three API endpoints for a query
  - Set up RabbitMQ to allow messages to post to a queue
  - Set up MailHog to allow use of Django's send_mail function to send a user an email
  - At the time an item is approved by admin:
    - The user receives an email that their item has posted
    - We compare the title of the posted item with all current queries to see if there are matches. If there are matches, the user will receive an email about the match.

  ## June 3, 2022
  - Created a messaging app based on example code from Learn
  - Set up the postgres database inside the messaging microservice
  - Wrote API endpoint in FastApi to write messages to database
  - Get and POST working for messages backend. Frontend and backend are connected as well.

  ## June 2, 2022
  - Wrote unit tests for API endpoints in inventory microservice
  - Added 'accept offer' and 'unaccept offer' functionality as API endpoints in inventory
  - Added get one item by item ID, get only items that are not yet sold as API endpoints
  - Created two offer endpoints to search for offers by user email address in sales microservice

  ## June 1, 2022
  - Created inventory model inside of the inventory microservice
  - Set up file paths to upload an image in Django
  - Wrote encoders for inventory models
  - Wrote four API endpoints for inventory microservice

## May 30, 2022
- Merged in messaging app branch into the main branch
- Created message model and migrated all changes

## May 26, 2022
- Created an inventory app in Django to track all of the Isella inventory
- Created a messaging app to accommodate the messaging component of the Isella app.

## May 25, 2022
- Set up Asana with different columns for the project

## May 24, 2022
- Wrote out all API endpoints for the App






