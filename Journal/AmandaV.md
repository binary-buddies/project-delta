## GHI handling

### 5/23/22 - 5/27/22

Began by creating images on Excelidraw to map out a visual of what the site could look like. I made images for the Main Page, Category Inventory page, User Dashboard, Messaging page and Favorites page.

Added these images into the docs/wireframes directory. 

Filled out the ghi.md file which contains a thorough description of each imported image. The image is linked to each description. 

## Set up React for project

### 5/27/22

I set up a ghi directory and installed react for the ISELLA project. I included an extra file, windows-setup.js, for complete usability amongst different os systems. I added run.sh so to include command lines to run during setup. Checked to make sure it was up and running without issues for the team.

## Added a Journal Directory & Installed Code Check Speller

### 5/28/22

Per instructions, I added a journal directory and added .md files for each contributor to log their progress. Installed Code Check Speller to my VSCode to help avoid grammatical errors in my markdown files.

Reviewed a contributors merge request.

## Worked on Accounts microservice

### 6/2/22

Worked on accounts microservice. Decided with group to leave the Accounts Microservice for another day due to Curtis releasing code with this information later this week. Made a new branch with items that had to be fixed up regardless for others to be able to have access to accounts microservice in docker. Pushed and submitted a merge request for these patches. 

## Made a Form to post an item

### 6/2/22

Made a form to post an item. Everything working except images section. Spent an hour working on it.


## List View Inventory - Pair Programming w/ Matty

### 6/3/22

Pair programmed with Matty with building a list view page for inventory. Handled merge conflicts with combining our front end components.

Reviewed incoming code from Jessica and handled in-line merge conflicts.

Continued to work on uploading images all day.

## Got images working in the Item Form and insomnia

### 6/4/22

Got the images to show on the main page. It turns out that the API endpoint needed to be edited in the list form. 

Got the File Uploader to work after ~15 hours of working on it. Tried multiple docs and videos. 
Biggest problems I encountered was how to get the file to upload correctly with the json serializer. In react, the answer was to follow the formData in docs. For python, I had to switch out the views from @http response to @api_view to handle the serializer. Per the video I watched, I made the serializer in another file and imported it. This was to allow for the formData and multi data inputs. 

Recap: Converted ItemsAdd.js Form to use a FormData object instead of using json serializer to include an image file. Converted Views.py to handle multipart FormData instead of Json so that it can handle the incoming image file. 

Updated the router paths in app.js for items and updated the new names in the existing pages (main page, nav bar, etc). 

Began working on a Detail Page.

#### More details about figuring out the image upload issue:
FRONT END:
File image issue going from the front to the back:
React made JSON, JSON sent to backend which is python, python interpreted the JSON but it just made "FILE" but the file contents weren't actually included in JSON. File is too big and complex to run through just the JSON serializer.

Needed to figure out how to send a file from the front end - used FormData Object for the solution here. It is a type of object that can handle a file. So instead of a JSON object, used a FormData object. Changed that in the React front-end page. 

Removed the header that had the "content type" header key that was JSON. I had tried to turn it to multipart form data, but that ended up causing an issue at the end. I had to comment that out to let the browser define that with the default properties in FormData. 

BACK END:
I had to alter the wrapper around GET & POST view to @api_view instead of @http methods so that I could alter the POST only. I had to change the post to receive formdata instead of JSON and parse it correctly. 

The serializer helps convert the formdata into an understandable format. This was directly followed by a video. 


Today:

go to authApi.js and add properties to (email) for fields for people to signup with 

Need to talk about paths 

Matty wants to talk about the dropdown sorter
    (maybe they can be side-by-side where you click one thing it sorts, then you sort by the next, then it populates the next options for that category)
    scaffolding?

Testing



## Create a detail page for the items

### 6/6/22

- Created a detail page for all items. The detail page handles the image, title, description, price, date posted, condition, retail price, a button for making an offer and a button for asking a question to the seller. 


## Revamp signup page to match website

### 6/8/22

- Styled the signup and signin pages to match the forms of the rest of the webpage.



## Add City, State and Zip to a User so Google API can be linked to a User's account

### 6/10/22

- Add City, State and Zip to a User so Google API can be linked to a User's account. Edits had to be made in the User model and the Signup form.


## Add City and State to an item so Google API can be linked to an item's posting location

### 6/13/22

Task: Add the city and state to an item, pre-populate in the form but allow it to be editable.

Added City and State inside the Item Model. Updated the Add Item Form to include fields for city and state. The field prepopulate with a default value of the user's city and state connected with their account, but it is editable so that the poster can change it if they would like. I also changed the seller information to no longer be an input field that can be manually typed it. It now uses the logged in users email address associated with their account and added to the item when the item is being created. I had to add the city and state fields to the accounts views to support the use of the fields dynamically in app.js. 

I then added city and state to the encoder for the detail view of an item. Then I opened the Items List One page to edit it and bring in the information collected for city and state so that it can be linked up to a google API by Matty in the detail page.



## Tied loose ends

### 6/14/22

- Hid the item add button on the ItemsListAll page so that if you're not logged in, you cannot click to add the button.
- Had a bug where the email was turning null when hard refreshing page for Item POST Form. Made a fix on the ItemAdd page to stop this from happening by adding a componentDidUpdate function to handle the sellers information populating.
- Fixed the offers modal to close out after submitting an offer. 



## Tied loose ends

### 6/15/22

- The items in each category needed to have an anchor tag to be able to be taken to a detail page.

- Added Anchor tags to the images for the category photos.


## Refactored Pages

### 6/16/22

- Refactored all pages for the cards to fill in correctly. A few category pages needed additional code to have them pulling in the information. 

- Clothing category page needed an additional file to support filtering through categories, not activities. 

- Made test cases for Offers. Test cases check to make sure the API endpoints are still linked up and in working condition. API endpoints check for getting all offers and also Posting an offer.

- Added all items to the main page using the export name in the ItemsListAll.js folder

## Edit User Form 

### 6/21/22

- made a hooks react form to edit user information. Had to add the fields to the api view to support auto populating information. I wasn't able to get the information to auto populate so I put in a help desk ticket. For right now, I wanted to make sure I at least had a working edit form up and running. I added the PUT method and verified everything was updating correctly. 

- Added the update user information button into the Dashboard Beta. 

- Noticed that a teammates code wasn't working as intended (button wasn't working), so I spent some time trying to see if I could get it to work. I wasn't able to, but will try to help tomorrow.

## Edit User Form - Auto Populate and Refresh 

### 6/22/22

-   Pair programmed with Curtis to get the Edit User form working on refresh so I could add the user's current city, state, and zip, without it breaking on refresh.

-   Assisted in a few large merge requests with many conflicts to sort through. 

-   Reformatted all React Pages to prepare for deployment

-   Deleted old code that was commented out to prepare for deployment

## Began Deployment 

### 6/23/22

-   Team began deployment. We started with the frontend: linter, tests and ghi react pages. The team was all hand on deck in a zoom room. We had many errors arise in our frontend where items were imported but never used, variables were defined but never used, images needed an alt tag associated with them, etc. As a team, we grabbed sections.

- After successful passing of the entire frontend, we each took a microservice for the backend. I took sales. I added deploy-sales-app-heroku and sales-app-heroku-release to the .gitlab-ci.yml file. I added a REACT_APP_SALES_API & 	
HEROKU_SALES_APP variable to gitlab CI/CD. I opened heroku and added config variables to the sales app in Heroku. In the sales microservice, I went to settings.py and added an os environment to allowed hosts.

-   I ran the build and everything passed.

-   We ran a total of ~70 tests in gitlab to get everything to pass