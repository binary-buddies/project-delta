# Antonio's Journal - 2022

## June 24
- DEPLOYMENT DAY, the remix. it took us about 10 hours yesterday to get to a pseudo-working state. as we encountered errors during the building phase we would divide and conquor which hopefully made it all move along more quickly. 
- We are so close to the finish line
- im trying to figure out why our acls app wont work on heroku. the build is fine but then it starts and immediatey crashes with error R10

## June 23
- DEPLOYMENT DAY!! feature freeze was last night. today we are each taking a few applications and running black and flake8

## June 22
- finished the edit item form (the errors had to do with a header issue that Curtis helped me with); not able to edit (remove and replace) item.image but everything else works!! Tinkering with the image thing but I don't see this making it to MVP

## June 21
- User info form passed to Amanda as we focus on MVP
- Item edit form is a challege because I've done this... just not using hooks
- had to make some changes to the Item model encoder (was missing properties)
- hitting two different errors, 405 or 415, which is stalling me at the 11th hour...this is so incredibly frustrating

## June 17
- communicated with team and dropped messaging api plan; this is not something I can exexute within the time constraints but Jessica said she might give it another whack if she has time
- picked up Item edit form and User info Edit form

## June 13 - June 16
- research and planning for messaging api; this gets more and more complex the more I map it out and I cannot find ANY examples to reference online. There are countless 'Facebook-style messaging app for React!' code-throughs but I have yet to find one that actually is that (they're all basically just chatrooms). I still haven't managed to wrap my head around this

## June 10
- started planning for an IM-style messaging interface that will connect sellers and buyers

## June 9
- endpoints for Rating FINISHED

## June 8
- GET endpoint for Rating

## June 7
- endpoints for Rating

## June 6
- User Rating Research
- Added Review model

## June 3
- updated API docs
- tested all views for Sales MS

## June 2
- added filter view by item endpoint
- tested polling; not a good solution for this application
- adjusted Offer models fields

## June 1
- finished Sales backend

## May 31
- debugging

## May 30
- coding models for sales; coding API endpoint for sales

## May 27
- began coding for models in sales microservice

## May 26
- created Sales app (django)

## May 24
- created data models for application