import { NavLink } from "react-router-dom";
import logo from "./Images/Isella-logo-purple-hat-small.png";
import SearchBar from "./SearchBar";

function Nav(props) {
  const token = props.token;
  const user = props.user;
  let staff = false;
  if (user) {
    staff = user.is_staff;
  }

  return (
    <>
      <div className="row">
        <div className="col-12">
          <nav className="navbar navbar-expand-lg blur border-radius-xl position-absolute my-3 top-0 border-bottom py-3 z-index-3 shadow my-3 py-2 start-0 end-0 mx-4">
            <div className="container">
              <NavLink className="navbar-brand" to="/">
                <img src={logo} alt="isella logo" width="60" />
              </NavLink>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <button
                className="navbar-toggler shadow-none ms-2"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navigation"
                aria-controls="navigation"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon mt-2">
                  <span className="navbar-toggler-bar bar1"></span>
                  <span className="navbar-toggler-bar bar2"></span>
                  <span className="navbar-toggler-bar bar3"></span>
                </span>
              </button>
              <div
                className="collapse navbar-collapse w-100 pt-3 pb-2 py-lg-0"
                id="navigation"
              >
                <ul className="navbar-nav navbar-nav-hover mx-auto">
                  {staff ? (
                    <>
                      <li className="nav-item1">
                        <NavLink to="/Admin" className="nav-link">
                          Admin
                        </NavLink>
                      </li>
                    </>
                  ) : (
                    <></>
                  )}
                  {token ? (
                    <>
                      <li className="nav-item1">
                        <NavLink to="/UserDashboardBeta" className="nav-link">
                          Dashboard
                        </NavLink>
                      </li>
                      <li className="nav-item1">
                        <NavLink to="/Logout" className="nav-link">
                          Logout
                        </NavLink>
                      </li>
                    </>
                  ) : (
                    <>
                      <li className="nav-item1">
                        <NavLink to="/UserSignup" className="nav-link">
                          Signup
                        </NavLink>
                      </li>
                      <li className="nav-item1">
                        <NavLink to="/UserLogin" className="nav-link">
                          {" "}
                          Login
                        </NavLink>
                      </li>
                    </>
                  )}
                </ul>
                {token ? (
                  <>
                    <ul className="navbar-nav d-lg-block d-none">
                      <li
                        className="btn btn-sm  bg-gradient-primary  mb-0 me-1"
                        role="button"
                      >
                        <NavLink
                          to="/items/add"
                          style={{ textDecoration: "none", color: "white" }}
                        >
                          Sell gear!
                        </NavLink>
                      </li>
                    </ul>
                  </>
                ) : (
                  <></>
                )}
                <SearchBar setSearchTerm={props.setSearchTerm} />
              </div>
            </div>
          </nav>
        </div>
      </div>
    </>
  );
}

export default Nav;
