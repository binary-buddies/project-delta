import "./Weather.css";
import React, { Component } from "react";

class Weather extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weather: "",
      wind: "",
      name: "",
      temp: "",
      icon: "",
      icon_url: "",
      citiesList: [],
      count: 0,
    };
  }
  async componentDidUpdate() {
    if (this.props.city && !this.state.citiesList.includes(this.props.city)) {
      let newCityList = this.state.citiesList;
      newCityList.push(this.props.city);
      this.setState({ citiesList: newCityList });
      this.setState({ count: this.state.count + 1 }, () => {

      });

      const url = `${process.env.REACT_APP_WEATHER_KEY}/api/weather?city=${this.props.city}&state=${this.props.state}`;
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        this.setState(
          {
            weather: data.weather[0].description,
            icon: data.weather[0].icon,
            temp: data.main.temp,
            name: data.name,
            wind: data.wind.speed,
          },
          () => {
            this.setState({
              icon_url: `http://openweathermap.org/img/w/${this.state.icon}.png`,
            });
          }
        );
      }
    }
  }

  render() {
    return (
      <>
        <div className="container-fluid ">
          <div className="row justify-content-center ">
            <div className="col-12 col-md-4 col-sm-12 col-xs-12">
              <div className="weather_card p-4 ">
                <div className="d-flex ">
                  <p>
                    {" "}
                    {""}
                    <strong>
                      {this.state.icon_url ? (
                        <img
                          src={this.state.icon_url}
                          alt="weather status icon"
                          className="weather-icon"
                        />
                      ) : null}
                      {(((this.state.temp - 273.15) * 9) / 5 + 32).toFixed(2)}
                      &deg; {`\n`}
                    </strong>
                    <strong>
                      {this.state.name}:{`\n`}
                    </strong>
                    {this.state.weather}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Weather;
