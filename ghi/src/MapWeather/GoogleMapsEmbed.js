import React from "react";
import Iframe from "react-iframe";

class GoogleMapsEmbed extends React.Component {

  render() {
    return (
      <div>
        <Iframe
          url={`https://www.google.com/maps/embed/v1/place?key=${process.env.REACT_APP_GOOGLE_MAPS_KEY}&q=${this.props.city}+${this.props.state}&zoom=12`}
          width="auto"
          height="350px"
          display="initial"
          position="relative"
        />
      </div>
    );
  }
}

export default GoogleMapsEmbed;
