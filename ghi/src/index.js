import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.css";
import { BrowserRouter } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById("root"));

async function loadData() {
  let AccountsData,
    ItemsData,
    MessagingData,
    OffersData,
    BrandsData,
    CategoriesData;
  const ItemsResponse = await fetch(
    `${process.env.REACT_APP_INVENTORY_API}/api/items/available/`
  );
  const OffersResponse = await fetch(
    `${process.env.REACT_APP_SALES_API}/api/offers/`
  );
  const BrandsResponse = await fetch(
    `${process.env.REACT_APP_INVENTORY_API}/api/manufacturers/`
  );
  const CategoriesResponse = await fetch(
    `${process.env.REACT_APP_INVENTORY_API}/api/categories/`
  );

  if (ItemsResponse.ok) {
    ItemsData = await ItemsResponse.json();
  } else {
    console.error(ItemsResponse);
  }

  if (OffersResponse.ok) {
    OffersData = await OffersResponse.json();
  } else {
    console.error.apply(OffersResponse);
  }

  if (CategoriesResponse.ok) {
    CategoriesData = await CategoriesResponse.json();
  } else {
    console.error.apply(CategoriesResponse);
  }

  if (BrandsResponse.ok) {
    BrandsData = await BrandsResponse.json();
  } else {
    console.error.apply(BrandsResponse);
  }

  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');

  root.render(
    <React.StrictMode>
      <BrowserRouter basename={basename}>
        <App
          offers={OffersData}
          messaging={MessagingData}
          items={ItemsData.items}
          accounts={AccountsData}
          brands={BrandsData}
          categories={CategoriesData}
        />
      </BrowserRouter>
    </React.StrictMode>
  );
}
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
loadData();
