import React from "react";
import { Link, Navigate  } from "react-router-dom";
 

class Admin extends React.Component {
  render() {
    if (!this.props.user) {
      return "Loading";
    }
    if (!this.props.user.is_staff) {
      return <Navigate to="/" />;
    }

    return (
      <>
        <div className="py-8">
          <div className="container">
            <h1 className="text-center">Admin</h1>
            <div className="row justify-space-between text-center py-2">
              <div className="col-8 mx-auto">
                <Link
                  to="/ItemsAddManufacturer"
                  className="btn bg-gradient-dark w-auto me-2"
                >
                  Add manufacturer
                </Link>
                <Link
                  to="/ActivitiesAdd"
                  className="btn bg-gradient-dark w-auto me-2"
                >
                  Add activity
                </Link>
                <Link
                  to="/CategoriesAdd"
                  className="btn bg-gradient-dark w-auto me-2"
                >
                  Add categories
                </Link>
                <Link
                  to="/Submitted"
                  className="btn bg-gradient-dark w-auto me-2"
                >
                  Submitted items
                </Link>
                <Link
                  to="/AdminAdmin"
                  className="btn bg-gradient-dark w-auto me-2"
                >
                  Admin access control
                </Link>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default Admin;
