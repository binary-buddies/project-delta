import React  from "react";
import { Navigate } from "react-router-dom";
import {  NavLink } from "react-router-dom";

class AdminAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      filter: "",
    };
    this.handleApproved = this.handleApproved.bind(this);
    this.handleNoneStaff = this.handleNoneStaff.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }
  async componentDidMount() {
    this.fetchData();
  }
  async fetchData() {
    
      const url = `${process.env.REACT_APP_ACCOUNT_API}/api/users/`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({ users: data.users });
      }
    
  }

  async handleNoneStaff(id) {
    const url = `${process.env.REACT_APP_ACCOUNT_API}/api/users/${id}/`;
    const fetchConfig = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ is_staff: false }),
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      this.fetchData();
    }
  }

  async handleApproved(id) {
    const approvedUrl = `${process.env.REACT_APP_ACCOUNT_API}/api/users/${id}/`;
    const fetchConfig = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ is_staff: true }),
    };
    const response = await fetch(approvedUrl, fetchConfig);
    if (response.ok) {
      this.fetchData();
    }
  }

  handleFilter(event) {
    const value = event.target.value;
    this.setState({ filter: value });
  }

  render() {
    if (!this.props.user) {
      return "Loading";
    }
    if (!this.props.user.is_staff) {
      return <Navigate to="/" />;
    }

    return (
      <>
        <div className="py-8">
          <div>
            <h1 className="text-center "> ADMIN ACCESS</h1>
          </div>
          <div className="text-center">
            <ul>
              <NavLink to="/Admin" className="navbar-Admin">
                Admin Home
              </NavLink>
            </ul>
          </div>

          <div className="input-group1  mx-auto mb-3">
            <input
              onChange={this.handleFilter}
              type="text"
              className="form-control"
              placeholder="enter email"
            />
          </div>

          <div className="mx-auto w-75">
            <table className="table text-center align-middle table-bordered table-hover  gx-5  ">
              <thead>
                <tr>
                  <th> ID </th>
                  <th> EMAIL </th>
                  <th> CITY </th>
                  <th> STATE </th>
                  <th> ZIP </th>
                  <th> ADMIN ACCESS </th>
                  <th> SUPERUSER</th>
                  <th> UPDATE ADMIN ACCESS</th>
                </tr>
              </thead>

              <tbody>
                {this.state.users.map((user) => {
                  let userApprove = "btn btn-outline-success btn-space";
                  let userDenied = "btn btn-outline-danger btn-space";
                  let approve_access = "Approve";
                  let denied_access = "Deny";

                  let toggle = "";
                  if (
                    user.email !== this.state.filter &&
                    this.state.filter !== ""
                  ) {
                    toggle = "d-none";
                  }

                  return (
                    <tr className={toggle} key={user.id}>
                      <td>{user.id}</td>
                      <td>{user.email}</td>
                      <td>{user.city}</td>
                      <td>{user.state}</td>
                      <td>{user.zip}</td>
                      <td>{user.is_staff ? "Approved" : ""}</td>
                      <td>{user.is_superuser ? "YES" : ""} </td>

                      <td>
                        {user.is_superuser ? (
                          <></>
                        ) : (
                          <>
                            <button
                              className={userApprove}
                              onClick={() => this.handleApproved(user.id)}
                              to=""
                            >
                              {approve_access}
                            </button>

                            <button
                              className={userDenied}
                              onClick={() => this.handleNoneStaff(user.id)}
                              to=""
                            >
                              {denied_access}
                            </button>
                          </>
                        )}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  }
}
export default AdminAdmin;
