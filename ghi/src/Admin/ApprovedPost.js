import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class ApprovedPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: [],
    };
    this.handleRejected = this.handleRejected.bind(this);
  }
  async componentDidMount() {
    this.fetchData();
  }
  async fetchData() {

    const url = `${process.env.REACT_APP_INVENTORY_API}/api/items/`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ item: data.item });
    }

  }
  async handleRejected(id) {
    const updateURl = `${process.env.REACT_APP_INVENTORY_API}/api/item/${id}/rejected/`;
    const fetchConfig = {
      method: "PUT",
      header: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(updateURl, fetchConfig);
    if (response.ok) {
      this.fetchData();
    }
  }

  render() {
    return (
      <>
        <div className="py-8">
          <h1 className="text-center"> Approved Post</h1>
          <nav
            className="navbar navbar-expand-md navbar-light bg-light bg-gradient bg-opacity-25"
            id="nav2"
          >
            <div className="gap-5 mx-auto">
              <ul>
                <NavLink to="/Admin" className="navbar-Admin">
                  Admin Home
                </NavLink>
                |
                <NavLink className="navbar-Admin" to="/Submitted">
                  Pending Post{" "}
                </NavLink>
              </ul>
            </div>
          </nav>
          <div className="mx-auto w-75">
            <table className="table text-center align-middle table-bordered table-hover">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Color</th>
                  <th>Size</th>
                  <th>Description</th>
                  <th> Image </th>
                </tr>
              </thead>
              <tbody>
                {this.state.item
                  .filter((item) => item.status === "Approved")
                  .map((item) => {

                    let status_rejected = "btn btn-outline-danger btn-space";


                    return (
                      <tr key={item.id}>
                        <td> {item.title} </td>
                        <td> {item.color} </td>
                        <td> {item.size} </td>
                        <td> {item.description}</td>
                        <td>
                          {item.image && (
                            <img
                              src={item.image}
                              width="200" alt=""
                            />
                          )}
                        </td>
                        <td>
                          <button
                            className={status_rejected}
                            onClick={() => this.handleRejected(item.id)}
                            to=""
                          >
                            Reject
                          </button>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  }
}
export default ApprovedPost;
