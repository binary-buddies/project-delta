import React from "react";

function MessageRow(props) {
  const when = new Date(props.message.timestamp);
  return (
    <tr>
      <td>{props.message.client_id}</td>
      <td>{when.toLocaleString()}</td>
      <td>{props.message.content}</td>
    </tr>
  );
}

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      // clientId: Number.parseInt(Math.random() * 10000000), // <- problem area
      clientID: "",
      connected: false,
      message: "",
    };
    this.sendMessage = this.sendMessage.bind(this);
    this.updateMessage = this.updateMessage.bind(this);
  }

  connect() {
    if (this.loading && !this.state.connected) {
      return;
    }
    this.loading = true;
    // Should be an environment variable in the future
    const url = `ws://localhost:8090/chat/${this.state.clientId}`;
    this.socket = new WebSocket(url);
    this.socket.addEventListener("open", () => {
      this.setState({ connected: true });
      this.loading = false;
    });
    this.socket.addEventListener("close", () => {
      this.setState({ connected: false });
      this.loading = false;
      setTimeout(() => {
        this.connect();
      }, 1000);
    });
    this.socket.addEventListener("error", () => {
      this.setState({ connected: false });
      this.loading = false;
      setTimeout(() => {
        this.connect();
      }, 1000);
    });
    this.socket.addEventListener("message", (message) => {
      this.setState({
        messages: [JSON.parse(message.data), ...this.state.messages],
      });
    });
  }

  componentDidMount() {
    const self = this;
    async function getUserData() {
      const url = `${process.env.REACT_APP_ACCOUNT_API}/api/users/me/`;
      const response = await fetch(url, { credentials: "include" });
      if (response.ok) {
        const data = await response.json();
        self.setState({
          username: data.username,
          clientId: data.id,
        });
      }
      self.connect();
    }
    getUserData();
  }

  sendMessage(e) {
    e.preventDefault();
    this.socket.send(this.state.message);
    this.setState({ message: "" });
  }

  updateMessage(e) {
    this.setState({ message: e.target.value });
  }

  render() {
    return (
      <>
        <h1>WebSocket Chat</h1>
        <h2>Your ID: {this.state.username || this.state.clientId}</h2>
        <form onSubmit={this.sendMessage}>
          <input
            value={this.state.message}
            className="form-control"
            type="text"
            id="messageText"
            autoComplete="off"
            onChange={this.updateMessage}
          />
          <button disabled={!this.state.connected} className="btn btn-primary">
            Send
          </button>
        </form>
        <h2>Messages</h2>
        <table className="table">
          <thead>
            <tr>
              <th>Client</th>
              <th>Date/Time</th>
              <th>Message</th>
            </tr>
          </thead>
          <tbody>
            {this.state.messages.map((message) => (
              <MessageRow
                key={message.username + message.timestamp}
                message={message}
              />
            ))}
          </tbody>
        </table>
      </>
    );
  }
}

export default Chat;
