import React, { Component } from "react";
import { Navigate } from "react-router-dom";

export default class UserLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
    };
    this.handleFormChange = this.handleFormChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleFormChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  async handleLogin() {
    const error = await this.props.login(
      this.state.username,
      this.state.password
    );
    this.setState({ error });
  }

  render() {
    if (this.props.token) {
      return <Navigate to="/UserDashBoardBeta" />;
    }
    return (
      <div className="py-8">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1> USER LOGIN PAGE</h1>
              <div>{this.state.error}</div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleFormChange}
                  value={this.state.username}
                  placeholder="Username"
                  required
                  type="text"
                  name="username"
                  id="username"
                  className="form-control"
                />
                <label htmlFor="username">Username</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleFormChange}
                  value={this.state.password}
                  placeholder="Password"
                  required
                  type="password"
                  name="password"
                  id="password"
                  className="form-control"
                />
                <label htmlFor="password">Password</label>
              </div>
              <button className="btn btn-primary" onClick={this.handleLogin}>
                Login
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
