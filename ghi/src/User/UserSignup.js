import React, { Component } from "react";
import { Navigate } from "react-router-dom";

export default class UserSignup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      email: "",
      first_name: "",
      last_name: "",
      city: "",
      state: "",
      zip: "",
    };
    this.handleFormChange = this.handleFormChange.bind(this);
    this.handleSignup = this.handleSignup.bind(this);
  }

  handleFormChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  async handleSignup(e) {
    e.preventDefault();
    const error = await this.props.signup(
      this.state.username,
      this.state.email,
      this.state.password,
      this.state.first_name,
      this.state.last_name,
      this.state.city,
      this.state.state,
      this.state.zip
    );
    this.setState({ error });
  }

  render() {
    if (this.props.token) {
      return <Navigate to="/UserDashBoardBeta" />;
    }
    return (
      <div className="py-8">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1> USER SIGNUP PAGE</h1>
              <div dangerouslySetInnerHTML={{ __html: this.state.error }} />
              <form onSubmit={this.handleSignup}>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFormChange}
                    value={this.state.username}
                    placeholder="Username"
                    required
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                  />
                  <label htmlFor="username">Username</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFormChange}
                    value={this.state.email}
                    placeholder="Email"
                    required
                    type="email"
                    name="email"
                    id="email"
                    className="form-control"
                  />
                  <label htmlFor="email">Email</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFormChange}
                    value={this.state.password}
                    placeholder="Password"
                    required
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                  />
                  <label htmlFor="password">Password</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFormChange}
                    value={this.state.first_name}
                    placeholder="First Name"
                    required
                    type="text"
                    name="first_name"
                    id="first_name"
                    className="form-control"
                  />
                  <label htmlFor="first_name">First Name</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFormChange}
                    value={this.state.last_name}
                    placeholder="Last Name"
                    required
                    type="text"
                    name="last_name"
                    id="last_name"
                    className="form-control"
                  />
                  <label htmlFor="last_name">Last Name</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFormChange}
                    value={this.state.city}
                    placeholder="City"
                    required
                    type="text"
                    name="city"
                    id="city"
                    className="form-control"
                  />
                  <label htmlFor="city">City</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFormChange}
                    value={this.state.state}
                    placeholder="State"
                    required
                    type="text"
                    name="state"
                    id="state"
                    className="form-control"
                  />
                  <label htmlFor="state">State</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFormChange}
                    value={this.state.zip}
                    placeholder="Zip"
                    required
                    type="text"
                    name="zip"
                    id="zip"
                    className="form-control"
                  />
                  <label htmlFor="zip">Zip</label>
                </div>

                <button className="btn btn-primary">Sign up</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
