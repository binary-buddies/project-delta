import { Navigate } from "react-router-dom";
import { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FloatingLabel from "react-bootstrap/FloatingLabel";

const UserEdit = (props) => {
  const { user } = props;
  const BASE_URL = `${process.env.REACT_APP_ACCOUNT_API}/api/users`;
  const [userCity, setCurrentUserCity] = useState("");
  const [userState, setCurrentUserState] = useState("");
  const [userZip, setCurrentUserZip] = useState("");
  const [submitted, setSubmitted] = useState(false);

  useEffect(() => {
    async function getUserData() {
      const url = `${BASE_URL}/${user.id}/`;

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        const userData = data.users;
        setCurrentUserCity(userData.city);
        setCurrentUserState(userData.state);
        setCurrentUserZip(userData.zip);
      }
    }
    if (user) {
      getUserData();
    }
  }, [user, BASE_URL]);

  // function handleChangeUserInfo(event) {
  //   const value = parseInt(event.target.value);
  //   setCurrentUserCity(value);
  //   setCurrentUserState(value);
  //   setCurrentUserZip(value);
  // }

  function handleChangeUserCity(event) {
    setCurrentUserCity(event.target.value);
  }

  function handleChangeUserState(event) {
    setCurrentUserState(event.target.value);
  }

  function handleChangeUserZip(event) {
    setCurrentUserZip(event.target.value);
  }

  async function handleUpdate(event) {
    event.preventDefault();

    const data = {
      city: userCity,
      state: userState,
      zip: userZip,
    };
    const url = `${BASE_URL}/${user.id}/`;

    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setSubmitted(true);
    }
  }

  if (submitted) {
    return <Navigate to="/UserDashboardBeta" />;
  }

  if (user) {
    return (
      <>
        <div className="py-8">
          <div className="container">
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Edit your account information</h1>
                  <Form onSubmit={handleUpdate}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <FloatingLabel
                        controlId="floatingInput"
                        label="City"
                        className="mb-3"
                      >
                        <Form.Control
                          type="text"
                          name="City"
                          onChange={handleChangeUserCity}
                          defaultValue={userCity}
                        />
                      </FloatingLabel>

                      <FloatingLabel
                        controlId="floatingInput"
                        label="State"
                        className="mb-3"
                      >
                        <Form.Control
                          type="numbers"
                          name="State"
                          onChange={handleChangeUserState}
                          defaultValue={userState}
                        />
                      </FloatingLabel>

                      <FloatingLabel
                        controlId="floatingInput"
                        label="Zip"
                        className="mb-3"
                      >
                        <Form.Control
                          type="text"
                          onChange={handleChangeUserZip}
                          defaultValue={userZip}
                        />
                      </FloatingLabel>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                      Update
                    </Button>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
};

export default UserEdit;
