import SaleItem from "./SaleItem";
import PendingSaleItem from "./PendingSaleItem";
import SoldItem from "./SoldItem";

export default function LargeSalesCard(props) {
  const { sale_items, pending_sale_items, sold_items } = props;

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-8 mx-auto text-center">
            <h3 className="text-gradient text-primary">Selling</h3>
            <p className="font-weight-normal">
              Take a look at all the items you are currently selling! If you
              have any offers, you can view and accept those here as well.
            </p>
          </div>
        </div>
        <div className="row">
          {sale_items.map((item) => (
            <SaleItem item={item} key={item.id}></SaleItem>
          ))}
        </div>
      </div>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-md-8 mx-auto text-center">
              <h3 className="text-gradient text-primary">Pending sales</h3>
              <p className="font-weight-normal">
                These are all the items that you've accepted an offer! You were
                sent the e-mail address of the buyer so you can coordinate your
                sale. If you need to unaccept the offer, go ahead and do that
                here. Don't forget to mark "sale complete" after you've received
                your cash.
              </p>
            </div>
          </div>
          <div className="col">
            <div className="container">
              <div className="row row-cols-1 row-cols-md-3 g-4">
                {pending_sale_items.map((pending_sale_item) => (
                  <PendingSaleItem
                    pending_sale_item={pending_sale_item}
                    key={pending_sale_item.id}
                  ></PendingSaleItem>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="py-2">
        <div className="container">
          <div className="row">
            <div className="col-md-8 mx-auto text-center">
              <h3 className="text-gradient text-primary">Sold</h3>
              <p className="font-weight-normal">
                Take a look at all the items you have sold with us!
              </p>
            </div>
          </div>
          <div className="col">
            <div className="container">
              <div className="row row-cols-1 row-cols-md-3 g-4">
                {sold_items.map((sold_item) => (
                  <SoldItem sold_item={sold_item} key={sold_item.id}></SoldItem>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
