export default function SmallCardTopOfDashboard(props) {
  const { title, data } = props;

  return (
    <>
      <div className="col">
        <div className="card border-info mb-3">
          <div className="card-body">
            <h5 className="card-title">{title}</h5>
            <h2>{data}</h2>
          </div>
        </div>
      </div>
    </>
  );
}
