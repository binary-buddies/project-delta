import { Link } from "react-router-dom";
import moment from "moment";

export default function Offer(props) {
  const { offer } = props;
  return (
    <>
      <section className="py-4">
        <div className="card bg-gradient-dark h-100">
          <div className="card-header bg-transparent text-sm-start text-center pt-4 pb-3 px-4">
            <h5 className="mb-1 text-white">{offer.item.title}</h5>
            <p className="mb-1 text-sm text-white">
              The seller is asking ${offer.item.price}
            </p>
            <p className="mb-1 text-sm text-white">
              You offered ${offer.price}
            </p>
            <p className="mb-3 text-sm text-white">
              You made this offer {moment(offer.created_at).fromNow()}
            </p>
            <Link
              to={`/items/${offer.item.id}`}
              style={{ textDecoration: "none", color: "black" }}
            >
              <button type="button" className="btn btn-light w-auto me-2">
                View item
              </button>
            </Link>
          </div>
        </div>
      </section>
    </>
  );
}
