import Alert from "./Alert";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import React, { useEffect, useState } from "react";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Popover from "react-bootstrap/Popover";
import Button from "react-bootstrap/Button";

export default function LargeAlertsCard(props) {
  const { alerts, user } = props;

  const [isOpen, setIsOpen] = useState(false);
  const [didClickAddNewAlertButton, setDidClickAddNewAlertButton] =
    useState(false);
  const [apiEndpoint, setApiEndpoint] = useState([]);
  const [alertTerm, setAlertTerm] = useState();
  const [userEmail, setUserEmail] = useState("");
  const BASE_URL = `${process.env.REACT_APP_INVENTORY_API}/api/queries/`;

  const handleSearchTermEntry = (event) => {
    const value = event.target.value;
    setAlertTerm(value);
  };

  const handleOnClickAddNewAlert = () => {
    setDidClickAddNewAlertButton(true);
    setApiEndpoint(BASE_URL);
  };

  const showModal = () => {
    setIsOpen(true);
    if (user) {
      setUserEmail(user.email);
    }
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  async function addAlert(url = "", data = {}) {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  useEffect(() => {
    if (didClickAddNewAlertButton === true) {
      addAlert(apiEndpoint, { query: alertTerm, user: userEmail });
      hideModal();
      window.location.reload();

      setDidClickAddNewAlertButton(false);
    }
  }, [didClickAddNewAlertButton, alertTerm, apiEndpoint, userEmail]);

  const alertPopover = (
    <Popover id="popover-basic">
      <Popover.Header as="h3">Alerts</Popover.Header>
      <Popover.Body>
        We will email you when an item is posted and the title contains the
        words in your alert!
      </Popover.Body>
    </Popover>
  );

  return (
    <>
      <section className="py-2">
        <div className="container">
          <div className="row">
            <div className="col-md-8 mx-auto text-center">
              <h4 className="text-gradient text-primary">
                Active alerts
                <OverlayTrigger
                  trigger="click"
                  placement="right"
                  overlay={alertPopover}
                >
                  <i className="bi bi-question-circle ml-2"></i>
                </OverlayTrigger>
              </h4>
            </div>
          </div>
          <div className="row justify-space-between text-center py-2">
            <div className="col-12 mx-auto">
              <button
                type="button"
                className="btn bg-gradient-primary w-auto me-2 mt-2"
                onClick={showModal}
              >
                Add alert
              </button>
            </div>
          </div>
          <div className="row justify-space-between text-center py-2">
            <div className="row row-cols-1 row-cols-md-5 gap-2 m-auto">
              {alerts.map((alert) => (
                <Alert alert={alert} key={alert.id}></Alert>
              ))}
            </div>
          </div>
        </div>
      </section>

      <Modal show={isOpen} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add a new alert</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Please type in a search term. We'll email you when something matching
          that term is posted.
        </Modal.Body>
        <Form className="ml-3">
          <Form.Group
            className="mb-3"
            display="inline-block"
            controlId="formBasicEmail"
          >
            <Form.Label className="ml-3">Alert term</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter alert term"
              onChange={handleSearchTermEntry}
            />
            <Form.Text className="text-muted">
              It's best to be specific. i.e. "patagonia jacket"
            </Form.Text>
          </Form.Group>
        </Form>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideModal}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleOnClickAddNewAlert}>
            Add
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
