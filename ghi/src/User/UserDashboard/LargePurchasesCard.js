import Offer from "./Offer";

export default function LargePurchasesCard(props) {
  const { offers, pending_offers, purchased } = props;

  return (
    <>
      <section className="py-4">
        <div className="container">
          <div className="row">
            <div className="col-md-8 mx-auto text-center">
              <h3 className="text-gradient text-primary">Offers</h3>
              <p className="font-weight-normal">
                These are all the items that you've offered to purchase! If the
                seller accepts your offer, you will be sent an email with the
                contact details of the seller.
              </p>
            </div>
          </div>
          <div className="col">
            <div className="container">
              <div className="row row-cols-1 row-cols-md-3 g-4">
                {offers.map((offer) => (
                  <Offer offer={offer} key={offer.id}></Offer>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-4">
        <div className="container">
          <div className="row">
            <div className="col-md-8 mx-auto text-center">
              <h3 className="text-gradient text-primary">Pending offers</h3>
              <p className="font-weight-normal">
                The seller has accepted your offer for these items! You should
                have received an email with the contact details of the seller.
                Please coordinate your sale with them!
              </p>
            </div>
          </div>
          <div className="col">
            <div className="container">
              <div className="row row-cols-1 row-cols-md-3 g-4">
                {pending_offers.map((pending_offer) => (
                  <Offer offer={pending_offer} key={pending_offer.id}></Offer>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-4 mb-8">
        <div className="container">
          <div className="row">
            <div className="col-md-8 mx-auto text-center">
              <h3 className="text-gradient text-primary">Purchased</h3>
              <p className="font-weight-normal">
                These are the items you've purchased!
              </p>
            </div>
          </div>
          <div className="col">
            <div className="container">
              <div className="row row-cols-1 row-cols-md-3 g-4">
                {purchased.map((purchased_offer) => (
                  <Offer
                    offer={purchased_offer}
                    key={purchased_offer.id}
                  ></Offer>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
