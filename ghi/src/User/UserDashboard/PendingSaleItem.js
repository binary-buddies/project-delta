import Button from "react-bootstrap/Button";
import React, { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";

function UnacceptOfferModal(props) {
  const { hideUnacceptModal, BASE_URL, pending_sale_item } = props;

  const [didClickUnacceptOffer, setDidClickUnacceptOffer] = useState(false);
  const [unacceptItemOfferApiEndpoint, setUnacceptItemOfferApiEndpoint] =
    useState();
  const [unacceptedOfferApiEndpoint, setUnacceptedOfferApiEndpoint] =
    useState();
  const BASE_SALES_URL = `${process.env.REACT_APP_SALES_API}/api/offers`;

  const handleOnClickUnacceptOffer = () => {
    setDidClickUnacceptOffer(true);
    setUnacceptItemOfferApiEndpoint(
      `${BASE_URL}/${pending_sale_item.id}/unaccept_offer/`
    );
    setUnacceptedOfferApiEndpoint(
      `${BASE_SALES_URL}/unaccepted/${pending_sale_item.accepted_offer}/`
    );
  };

  async function unacceptOfferOnItem(url = "") {
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    });
    return response.json();
  }

  useEffect(() => {
    if (didClickUnacceptOffer) {
      unacceptOfferOnItem(unacceptItemOfferApiEndpoint);
      unacceptOfferOnItem(unacceptedOfferApiEndpoint);
      hideUnacceptModal();
      window.location.reload();
    }
  }, [
    didClickUnacceptOffer,
    unacceptItemOfferApiEndpoint,
    unacceptedOfferApiEndpoint,
    hideUnacceptModal
  ]);

  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title>Unaccept offer</Modal.Title>
      </Modal.Header>
      <Modal.Body>Are you sure you want to unaccept this offer?</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={hideUnacceptModal}>
          Cancel
        </Button>
        <Button variant="primary" onClick={handleOnClickUnacceptOffer}>
          Unaccept
        </Button>
      </Modal.Footer>
    </>
  );
}
export default function PendingSaleItem(props) {
  const { pending_sale_item } = props;

  const [didClickSaleComplete, setDidClickSaleComplete] = useState(false);
  const [apiEndpoint, setApiEndpoint] = useState();
  const [finalOfferApiEndpoint, setFinalOfferApiEndpoint] = useState();
  const BASE_URL = `${process.env.REACT_APP_INVENTORY_API}/api/items`;
  const [isOpen, setIsOpen] = useState(false);
  const [unacceptIsOpen, setUnacceptIsOpen] = useState(false);

  const handleOnClickSaleComplete = () => {
    setDidClickSaleComplete(true);
    setApiEndpoint(`${BASE_URL}/mark_sold/${pending_sale_item.id}/`);
    setFinalOfferApiEndpoint(
      `${process.env.REACT_APP_SALES_API}/api/offers/sold/${pending_sale_item.accepted_offer}/`
    );
  };

  const showUnacceptModal = () => {
    setUnacceptIsOpen(true);
  };

  const hideUnacceptModal = () => {
    setUnacceptIsOpen(false);
  };

  const showModal = () => {
    setIsOpen(true);
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  async function completeSale(url = "") {
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    });
    return response.json();
  }

  useEffect(() => {
    if (didClickSaleComplete) {
      completeSale(apiEndpoint);
      completeSale(finalOfferApiEndpoint);
      hideModal();
      window.location.reload();
    }
  }, [didClickSaleComplete, apiEndpoint, finalOfferApiEndpoint]);

  return (
    <>
      <div className="col-lg-4 col-md-6">
        <div className="col-8 mx-auto">
          <div className="card card-plain card-blog mt-4 border-0">
            <div className="card-image border-radius-lg position-relative">
              <div className="blur-shadow-image">
                <img
                  className="img border-radius-lg move-on-hover"
                  src={pending_sale_item.image}
                  alt=""
                />
              </div>
            </div>
            <div className="card-body">
              <h6 className="text-center mb-3">{pending_sale_item.title}</h6>
              <p className="mb-2 text-sm">Asking: ${pending_sale_item.price}</p>
              <div className="">
                <button
                  type="button"
                  className="btn bg-gradient-secondary btn-sm"
                  onClick={showUnacceptModal}
                >
                  Unaccept offer
                </button>
                <button
                  type="button"
                  className="btn bg-gradient-primary btn-sm"
                  onClick={showModal}
                >
                  Sale complete
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Modal show={isOpen} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>Finalize sale</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Please complete your sale by selecting the 'sale complete' button.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideModal}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleOnClickSaleComplete}>
            Sale complete
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={unacceptIsOpen} onHide={hideUnacceptModal}>
        <UnacceptOfferModal
          hideUnacceptModal={hideUnacceptModal}
          BASE_URL={BASE_URL}
          pending_sale_item={pending_sale_item}
        ></UnacceptOfferModal>
      </Modal>
    </>
  );
}
