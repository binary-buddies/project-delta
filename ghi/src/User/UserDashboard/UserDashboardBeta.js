import { Link } from "react-router-dom";
import React, { Component } from "react";
import LargeSalesCard from "./LargeSalesCard";
import LargeAlertsCard from "./LargeAlertsCard";
import LargePurchasesCard from "./LargePurchasesCard";
import Button from "react-bootstrap/Button";

export default class UserDashboardBeta extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sale_items: [],
      pending_sale_items: [],
      sold_items: [],
      offers: [],
      pending_offers: [],
      purchased: [],
      alerts: [],
      alert_number: 0,
      revenue: 0,
      all_items: props.all_items,
      user: this.props.user,
      sales_showing: false,
      purchases_showing: false,
      alerts_showing: true,
    };
    this.handleClickOnSales = this.handleClickOnSales.bind(this);
    this.handleClickOnPurchases = this.handleClickOnPurchases.bind(this);
    this.handleClickOnAlerts = this.handleClickOnAlerts.bind(this);
  }
  async getItemsForSale() {
    if (this.props.user) {
      const salesUrl = `${process.env.REACT_APP_INVENTORY_API}/api/items/available/${this.props.user.email}/`;
      try {
        const response = await fetch(salesUrl);
        if (response.ok) {
          const salesData = await response.json();
          this.setState({ sale_items: salesData.items });
        }
      } catch (e) { }
      return false;
    }
  }

  async getItemsPendingSale() {
    if (this.props.user) {
      const pendingSalesUrl = `${process.env.REACT_APP_INVENTORY_API}/api/items/pending_sales/${this.props.user.email}/`;
      try {
        const response = await fetch(pendingSalesUrl);
        if (response.ok) {
          const pendingSaleData = await response.json();
          this.setState({ pending_sale_items: pendingSaleData.items });
        }
      } catch (e) { }
      return false;
    }
  }

  async getItemsSold() {
    if (this.props.user) {
      const soldItemsUrl = `${process.env.REACT_APP_INVENTORY_API}/api/items/sold/${this.props.user.email}/`;
      try {
        const response = await fetch(soldItemsUrl);
        if (response.ok) {
          const soldItemsData = await response.json();
          this.setState({ sold_items: soldItemsData.items });
          this.setState({ revenue: soldItemsData.revenue });
        }
      } catch (e) { }
      return false;
    }
  }

  async getAlerts() {
    if (this.props.user) {
      const alertsUrl = `${process.env.REACT_APP_INVENTORY_API}/api/queries/${this.props.user.email}/`;
      try {
        const response = await fetch(alertsUrl);
        if (response.ok) {
          const alertsData = await response.json();
          this.setState({
            alerts: alertsData.queries,
            alert_number: alertsData.count,
          });
        }
      } catch (e) { }
      return false;
    }
  }

  async getOffers(url, var_name) {
    if (this.props.user) {
      try {
        const response = await fetch(url);
        if (response.ok) {
          const offersData = await response.json();
          if (var_name === "offers") {
            this.setState({ offers: offersData.offers });
          }
          if (var_name === "pendingOffers") {
            this.setState({ pending_offers: offersData.offers });
          }
          if (var_name === "purchased") {
            this.setState({ purchased: offersData.offers });
          }
        }
      } catch (e) { }
      return false;
    }
  }

  componentDidMount() {
    if (this.props.user) {
      const BASE_OFFERS_URL = `${process.env.REACT_APP_SALES_API}/api/offers/buyer`;
      const offersUrl = `${BASE_OFFERS_URL}/${this.props.user.email}/`;
      const pendingOffersUrl = `${BASE_OFFERS_URL}/pending_offers/${this.props.user.email}/`;
      const finalOffersUrl = `${BASE_OFFERS_URL}/final_offers/${this.props.user.email}/`;
      this.setState({ user: this.props.user });
      this.getItemsForSale();
      this.getItemsPendingSale();
      this.getItemsSold();
      this.getAlerts();
      this.getOffers(offersUrl, "offers");
      this.getOffers(pendingOffersUrl, "pendingOffers");
      this.getOffers(finalOffersUrl, "purchased");
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.user !== prevProps.user) {
      const BASE_OFFERS_URL = `${process.env.REACT_APP_SALES_API}/api/offers/buyer`;
      const offersUrl = `${BASE_OFFERS_URL}/${this.props.user.email}/`;
      const pendingOffersUrl = `${BASE_OFFERS_URL}/pending_offers/${this.props.user.email}/`;
      const finalOffersUrl = `${BASE_OFFERS_URL}/final_offers/${this.props.user.email}/`;
      this.getItemsForSale();
      this.getItemsPendingSale();
      this.getItemsSold();
      this.getAlerts();
      this.getOffers(offersUrl, "offers");
      this.getOffers(pendingOffersUrl, "pendingOffers");
      this.getOffers(finalOffersUrl, "purchased");
      this.setState({ user: this.props.user });
    }
  }

  handleClickOnSales() {
    this.setState({ sales_showing: true });
    this.setState({ purchases_showing: false });
    this.setState({ alerts_showing: false });
  }

  handleClickOnPurchases() {
    this.setState({ sales_showing: false });
    this.setState({ purchases_showing: true });
    this.setState({ alerts_showing: false });
  }

  handleClickOnAlerts() {
    this.setState({ sales_showing: false });
    this.setState({ purchases_showing: false });
    this.setState({ alerts_showing: true });
  }

  render() {
    if (!this.props.user) {
      return (
        <>
          <div className="container">
            <div>Loading</div>
          </div>
        </>
      );
    }
    var salesClasses = "d-none";
    var purchaseClasses = "d-none";
    var alertClasses = "";
    if (this.state.sales_showing) {
      salesClasses = "";
      purchaseClasses = "d-none";
      alertClasses = "d-none";
    }
    if (this.state.purchases_showing) {
      salesClasses = "d-none";
      purchaseClasses = "";
      alertClasses = "d-none";
    }
    if (this.state.alerts_showing) {
      salesClasses = "d-none";
      purchaseClasses = "d-none";
      alertClasses = "";
    }
    return (
      <>
        <section className="py-8 mt-4">
          <div className="container">
            <div className="row text-center">
              <div className="col-lg-4 col-md-6">
                <div className="info">
                  <h1>
                    <i className="bi bi-star-fill"></i>
                  </h1>
                  <h5 className="mt-3">Your rating</h5>
                  <p>No ratings yet!</p>
                </div>
              </div>
              <div className="col-lg-4 col-md-6">
                <div className="info">
                  <h1>
                    <i className="bi bi-exclamation-circle lg"></i>
                  </h1>
                  <h5 className="mt-3">Active alerts</h5>
                  <p>You have {this.state.alert_number} active alerts.</p>
                </div>
              </div>
              <div className="col-lg-4 col-md-6">
                <div className="info">
                  <h1>
                    <i className="bi bi-currency-dollar"></i>
                  </h1>
                  <h5 className="mt-3">Revenue</h5>
                  <p>You have made ${this.state.revenue} dollars.</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            <div className="row justify-space-between text-center py-2">
              <div className="col-8 mx-auto">
                <Button
                  onClick={this.handleClickOnAlerts}
                  className="btn bg-gradient-dark w-auto me-2"
                >
                  My alerts
                </Button>
                <Button
                  onClick={this.handleClickOnSales}
                  className="btn bg-gradient-dark w-auto me-2"
                >
                  My sales
                </Button>
                <Button
                  onClick={this.handleClickOnPurchases}
                  className="btn bg-gradient-dark w-auto me-2"
                >
                  My purchases
                </Button>
                <Link
                  to="/UserEdit"
                  className="btn bg-gradient-dark w-auto me-2"
                >
                  Update Account
                </Link>
              </div>
            </div>
          </div>
        </section>
        <div className={alertClasses}>
          <section className="py-3 mt-2 mb-8">
            <div className="container">
              <LargeAlertsCard
                alerts={this.state.alerts}
                user={this.state.user}
              ></LargeAlertsCard>
            </div>
          </section>
        </div>
        <div className={salesClasses}>
          <section className="py-3 mt-2 mb-8">
            <LargeSalesCard
              sale_items={this.state.sale_items}
              pending_sale_items={this.state.pending_sale_items}
              sold_items={this.state.sold_items}
            ></LargeSalesCard>
          </section>
        </div>

        <div className={purchaseClasses}>
          <LargePurchasesCard
            offers={this.state.offers}
            pending_offers={this.state.pending_offers}
            purchased={this.state.purchased}
            items={this.state.all_items}
          ></LargePurchasesCard>
        </div>
      </>
    );
  }
}
