import Card from "react-bootstrap/Card";
import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { Link } from "react-router-dom";

function OfferLineItem(props) {
  const { item, offer, hideModal } = props;
  const [apiEndpoint, setApiEndpoint] = useState();
  const [salesApiEndpoint, setSalesApiEndpoint] = useState();
  const [didClickAcceptOffer, setDidClickAcceptOffer] = useState(false);

  const BASE_URL = `${process.env.REACT_APP_INVENTORY_API}/api/items`;
  const BASE_SALES_URL = `${process.env.REACT_APP_SALES_API}/api/offers/accepted`;

  const handleOnClickAcceptOffer = () => {
    setDidClickAcceptOffer(true);
    setApiEndpoint(`${BASE_URL}/${item.id}/accept_offer/`);
    setSalesApiEndpoint(`${BASE_SALES_URL}/${offer.id}/`);
  };

  async function acceptOffer(url = "", data = {}) {
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  async function offerAcceptedAt(url = "") {
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    });
    return response.json();
  }

  useEffect(() => {
    if (didClickAcceptOffer) {
      acceptOffer(apiEndpoint, {
        accepted_offer: offer.id,
        buyer_email: offer.buyer,
      });
      offerAcceptedAt(salesApiEndpoint);
      hideModal();
      window.location.reload();
    }
  }, [offer.id, didClickAcceptOffer, apiEndpoint, salesApiEndpoint, hideModal, offer.buyer]);

  return (
    <>
      <Card
        style={{ marginBottom: "15px", marginTop: "15px", cursor: "pointer" }}
      >
        <Card.Body>
          <span>
            {offer.buyer} offered you ${offer.price}!
          </span>
          <Button
            className="btn btn-warning pull-right"
            onClick={handleOnClickAcceptOffer}
          >
            Accept
          </Button>
        </Card.Body>
      </Card>
    </>
  );
}

function OffersModal(props) {
  const { item, hideModal } = props;

  const [offers, setOffers] = useState([]);

  useEffect(() => {
    async function getOffers() {
      const BASE_URL = `${process.env.REACT_APP_SALES_API}/api/offers/item`;
      const offersUrl = `${BASE_URL}/${item.id}/`;
      try {
        const response = await fetch(offersUrl);
        if (response.ok) {
          const offersData = await response.json();
          setOffers(offersData.offers);
        }
      } catch (e) { }
      return false;
    }
    getOffers();
  }, [item.id]);

  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title>Offers on {item.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container">
          {offers.map((offer) => (
            <OfferLineItem
              item={item}
              offer={offer}
              key={offer.id}
              hideModal={hideModal}
            ></OfferLineItem>
          ))}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={hideModal}>
          Cancel
        </Button>
      </Modal.Footer>
    </>
  );
}

export default function SaleItem(props) {
  const { item } = props;
  const [isOpen, setIsOpen] = useState(false);
  const [didClickViewOffersButton, setDidClickViewOffersButton] = useState(false);

  const handleOnClickViewOffers = () => {
    setDidClickViewOffersButton(true);
    console.log(didClickViewOffersButton)
    setIsOpen(true);
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  return (
    <>
      <div className="col-lg-4 col-md-6">
        <div className="col-8 mx-auto">
          <div className="card card-plain card-blog mt-4 border-0">
            <div className="card-image border-radius-lg position-relative">
              <div className="blur-shadow-image">
                <img
                  className="img border-radius-lg move-on-hover"
                  src={item.image}
                  alt="" />
              </div>
            </div>
            <div className="card-body">
              <h6 className="text-center mb-3">{item.title}</h6>
              <p className="mb-2 text-sm">Asking: ${item.price}</p>
              <div className="">
                <button
                  type="button"
                  className="btn bg-gradient-secondary btn-sm"
                  onClick={handleOnClickViewOffers}
                >
                  View offers
                </button>
                <Link
                  to={`/items/update/${item.id}`}
                  style={{ textDecoration: "none", color: "black" }}
                  key={item.id}
                >
                  <button
                    type="button"
                    className="btn bg-gradient-primary btn-sm"
                  >
                    Edit item
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Modal show={isOpen} onHide={hideModal}>
        <OffersModal item={item} hideModal={hideModal}></OffersModal>
      </Modal>
    </>
  );
}
