export default function SoldItem(props) {
  const { sold_item } = props;
  return (
    <>
      <div className="col-lg-4 col-md-6">
        <div className="col-8 mx-auto">
          <div className="card card-plain card-blog mt-4 border-0">
            <div className="card-image border-radius-lg position-relative">
              <div className="blur-shadow-image">
                <img
                  className="img border-radius-lg move-on-hover"
                  src={sold_item.image}
                  alt=""
                />
              </div>
            </div>
            <div className="card-body">
              <h6 className="text-center mb-3">{sold_item.title}</h6>
              <div className="">
                <button
                  type="button"
                  className="btn bg-gradient-primary btn-sm"
                >
                  Rate buyer
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
