import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import moment from "moment";

export default function Alert(props) {
  const { alert } = props;

  const [isOpen, setIsOpen] = useState(false);
  const [didClickDeleteButton, setDidClickDeleteButton] = useState(false);
  const [apiEndpoint, setApiEndpoint] = useState([]);
  const BASE_URL = `${process.env.REACT_APP_INVENTORY_API}/api/query/`;
  const navigate = useNavigate();

  async function deleteThisAlert(url = "") {
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });
    return response.json();
  }

  useEffect(() => {
    if (didClickDeleteButton === true) {
      deleteThisAlert(apiEndpoint);
      setDidClickDeleteButton(false);
      navigate("/UserDashboardBeta");
    }
  }, [didClickDeleteButton, apiEndpoint, navigate]);

  const handleOnDelete = () => {
    setDidClickDeleteButton(true);
    setApiEndpoint(`${BASE_URL}${alert.id}/`);
    setIsOpen(false);
    window.location.reload();
  };

  const showModal = () => {
    setIsOpen(true);
  };

  const hideModal = () => {
    setIsOpen(false);
  };
  return (
    <>
      <div className="card">
        <div className="card-body pt-3">
          <p className="text-dark mb-2 text-sm">Alert</p>
          <h5>"{alert.query}"</h5>
          <p>Created: {moment(alert.created_at).fromNow()}</p>
          <button
            type="button"
            className="btn btn-outline-primary btn-sm mb-0"
            onClick={showModal}
          >
            Delete alert
          </button>
        </div>
      </div>

      <Modal show={isOpen} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>Delete alert</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you would like to delete the alert: "{alert.query}"?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideModal}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleOnDelete}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
