import React from "react";

class InventoryForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      image: "",
      category: "",
      categories: [],
      activity: "",
      activities: [],
      manufacturer: "",
      manufacturers: [],
      condition: "",
      conditions: [
        "New",
        "Reconditioned",
        "Open box (never used)",
        "Used (normal wear)",
        "For parts",
        "Other (see description)",
      ],
      color: "",
      size: "",
      description: "",
      msrp: "",
      price: "",
      hasSignedUp: false,
    };

    if (this.props.user) {
      this.state.seller = this.props.user.email;
      this.state.city = this.props.user.city;
      this.state.state = this.props.user.state;
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeCity = this.handleChangeCity.bind(this);
    this.handleChangeState = this.handleChangeState.bind(this);
    this.handleChangeCategory = this.handleChangeCategory.bind(this);
    this.handleChangeActivity = this.handleChangeActivity.bind(this);
    this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
    this.handleChangeCondition = this.handleChangeCondition.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangeSize = this.handleChangeSize.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeMSRP = this.handleChangeMSRP.bind(this);
    this.handleChangePrice = this.handleChangePrice.bind(this);
    this.handleChangeImage = this.handleChangeImage.bind(this);
  }

  async componentDidMount() {
    {
      const categoryUrl = `${process.env.REACT_APP_INVENTORY_API}/api/categories/`;

      const response = await fetch(categoryUrl);

      if (response.ok) {
        const data = await response.json();
        this.setState({ categories: data.categories });
      }
    }

    {
      const activityUrl = `${process.env.REACT_APP_INVENTORY_API}/api/activities/`;

      const response = await fetch(activityUrl);

      if (response.ok) {
        const data = await response.json();
        this.setState({ activities: data.activities });
      }
    }

    {
      const manufacturerUrl = `${process.env.REACT_APP_INVENTORY_API}/api/manufacturers/`;

      const response = await fetch(manufacturerUrl);

      if (response.ok) {
        const data = await response.json();

        this.setState({ manufacturers: data.brands });
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.user !== prevProps.user) {
      this.setState({
        seller: this.props.user.email,
        city: this.props.user.city,
        state: this.props.user.state,
      });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.categories;
    delete data.activities;
    delete data.manufacturers;
    delete data.conditions;
    delete data.hasSignedUp;

    // this creates the formData object that includes the image file
    let formData = new FormData();
    // this is the for loop to convert our state data to the formData object
    for (const name in data) {
      if (name === "image") {
        formData.append(name, data[name], data[name].name);
      } else {
        formData.append(name, data[name]);
      }
    }

    const modelUrl = `${process.env.REACT_APP_INVENTORY_API}/api/items/`;
    const fetchConfig = {
      method: "post",
      // uses the formData object instead of JSON
      body: formData,
    };
    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      this.setState({
        title: "",
        image: "",
        category: "",
        categories: [],
        activity: "",
        activities: [],
        manufacturer: "",
        manufacturers: [],
        condition: "",
        conditions: [
          "New",
          "Reconditioned",
          "Open box (never used)",
          "Used (normal wear)",
          "For parts",
          "Other (see description)",
        ],
        color: "",
        size: "",
        description: "",
        msrp: "",
        price: "",
        hasSignedUp: true,
      });
    }
  }

  handleChangeTitle(event) {
    const value = event.target.value;
    this.setState({ title: value });
  }

  handleChangeCity(event) {
    const value = event.target.value;
    this.setState({ city: value });
  }

  handleChangeState(event) {
    const value = event.target.value;
    this.setState({ state: value });
  }

  handleChangeCategory(event) {
    const value = event.target.value;
    this.setState({ category: value });
  }

  handleChangeActivity(event) {
    const value = event.target.value;
    this.setState({ activity: value });
  }

  handleChangeManufacturer(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  handleChangeCondition(event) {
    const value = event.target.value;
    this.setState({ condition: value });
  }

  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleChangeSize(event) {
    const value = event.target.value;
    this.setState({ size: value });
  }

  handleChangeDescription(event) {
    const value = event.target.value;
    this.setState({ description: value });
  }

  handleChangeMSRP(event) {
    const value = event.target.value;
    this.setState({ msrp: value });
  }

  handleChangePrice(event) {
    const value = event.target.value;
    this.setState({ price: value });
  }

  handleChangeImage(event) {
    const value = event.target.files[0];
    this.setState({ image: value });
  }

  render() {
    let messageClasses = "alert alert-success d-none mb-0";
    let formClasses = "";
    if (this.state.hasSignedUp) {
      messageClasses = "alert alert-success mb-0";
      formClasses = "d-none";
    }

    return (
      <div className="py-8">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a new Item</h1>
              <form
                className={formClasses}
                onSubmit={this.handleSubmit}
                id="create-model-form"
              >
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeTitle}
                    placeholder="Title"
                    required
                    type="text"
                    name="title"
                    id="title"
                    className="form-control"
                  />
                  <label htmlFor="title">Title</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeCity}
                    placeholder="City"
                    required
                    type="text"
                    name="city"
                    id="city"
                    className="form-control"
                    value={`${this.state.city}`}
                  />
                  <label htmlFor="city">City</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeState}
                    placeholder="State"
                    required
                    type="text"
                    name="state"
                    id="state"
                    className="form-control"
                    value={`${this.state.state}`}
                  />
                  <label htmlFor="state">State</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeImage}
                    placeholder="Image"
                    required
                    type="file"
                    name="image"
                    id="image"
                    className="form-control"
                  />
                  <label htmlFor="image">Image</label>
                </div>

                <div className="mb-3">
                  <select
                    onChange={this.handleChangeCategory}
                    required
                    name="category"
                    id="manufacturer"
                    className="form-select"
                  >
                    <option value="">Select a Category</option>
                    {this.state.categories.map((category) => {
                      return (
                        <option key={category.id} value={category.id}>
                          {category.name}
                        </option>
                      );
                    })}
                  </select>
                </div>

                <div className="mb-3">
                  <select
                    onChange={this.handleChangeActivity}
                    required
                    name="activity"
                    id="activity"
                    className="form-select"
                  >
                    <option value="">Select an Activity</option>
                    {this.state.activities.map((activity) => {
                      return (
                        <option key={activity.id} value={activity.id}>
                          {activity.name}
                        </option>
                      );
                    })}
                  </select>
                </div>

                <div className="mb-3">
                  <select
                    onChange={this.handleChangeManufacturer}
                    required
                    name="manufacturer"
                    id="manufacturer"
                    className="form-select"
                  >
                    <option value="">Select a Manufacturer</option>
                    {this.state.manufacturers.map((manufacturer) => {
                      return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                          {manufacturer.name}
                        </option>
                      );
                    })}
                  </select>
                </div>

                <div className="mb-3">
                  <select
                    onChange={this.handleChangeCondition}
                    required
                    name="condition"
                    id="condition"
                    className="form-select"
                  >
                    <option value="">Choose the condition</option>
                    {this.state.conditions.map((condition) => {
                      return (
                        <option key={condition} value={condition}>
                          {condition}
                        </option>
                      );
                    })}
                  </select>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeColor}
                    placeholder="Color"
                    required
                    type="text"
                    name="color"
                    id="color"
                    className="form-control"
                  />
                  <label htmlFor="color">Color</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeSize}
                    placeholder="Size"
                    required
                    type="text"
                    name="size"
                    id="size"
                    className="form-control"
                  />
                  <label htmlFor="color">Size</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeDescription}
                    placeholder="Description"
                    required
                    type="text"
                    name="description"
                    id="description"
                    className="form-control"
                  />
                  <label htmlFor="description">Description</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangeMSRP}
                    placeholder="MSRP"
                    required
                    type="decimal"
                    name="msrp"
                    id="msrp"
                    className="form-control"
                  />
                  <label htmlFor="msrp">MSRP</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleChangePrice}
                    placeholder="Price"
                    required
                    type="decimal"
                    name="price"
                    id="price"
                    className="form-control"
                  />
                  <label htmlFor="price">List Price</label>
                </div>

                <button className="btn btn-primary">Add Item</button>
              </form>
              <br />
              <div className={messageClasses} id="success-message">
                Your Item Has Been Posted!
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default InventoryForm;
