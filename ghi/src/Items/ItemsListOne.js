import React from "react";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import moment from "moment";
import Weather from "../MapWeather/Weather";
import Iframe from "../MapWeather/GoogleMapsEmbed";
import NavToPages from "../NavToPages";
import ScrollToTop from "../ScrollToTop";

const ItemsListOne = (props) => {
  const { itemId } = useParams();
  const BASE_URL = `${process.env.REACT_APP_INVENTORY_API}/api/items`;
  const [itemTitle, setCurrentItemTitle] = useState([]);
  const [itemDescription, setCurrentItemDescription] = useState([]);
  const [itemImage, setCurrentItemImage] = useState([]);
  const [itemPrice, setCurrentItemPrice] = useState([]);
  const [itemMsrp, setCurrentItemMsrp] = useState([]);
  const [itemCreatedAt, setCurrentCreatedAt] = useState([]);
  const [itemCondition, setCurrentItemCondition] = useState([]);
  const [itemSize, setItemSize] = useState([]);
  const [itemColor, setItemColor] = useState([]);
  const [itemCity, setCurrentItemCity] = useState();
  const [itemState, setCurrentItemState] = useState();
  const [offerPrice, setOfferPrice] = useState("");
  const [itemSeller, setItemSeller] = useState();
  const [currentUser, setCurrentUser] = useState();

  useEffect(() => {
    const itemUrl = `${BASE_URL}/${itemId}/`;
    fetch(itemUrl)
      .then((res) => res.json())
      .then((data) => {
        setCurrentItemTitle(data.title);
        setCurrentItemDescription(data.description);
        setCurrentItemImage(
          data.image
        );
        setCurrentItemPrice(data.price);
        setCurrentItemMsrp(data.msrp);
        setCurrentCreatedAt(moment(data.created_at).fromNow());
        setCurrentItemCondition(data.condition);
        setCurrentItemCity(data.city);
        setCurrentItemState(data.state);
        setItemSeller(data.seller);
        setCurrentUser(props.user);
        setItemSize(data.size);
        setItemColor(data.color);
      });
  }, [itemId, props.user, BASE_URL]);

  function handleChangeOffer(event) {
    const value = parseInt(event.target.value);
    setOfferPrice(value);
  }

  async function handleOffer(event) {
    event.preventDefault();
    const data = {
      item: parseInt(itemId),
      seller: itemSeller,
      price: offerPrice,
      buyer: currentUser.email,
    };

    const offersUrl = `${process.env.REACT_APP_SALES_API}/api/offers/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
    };
    const response = await fetch(offersUrl, fetchConfig);
  }

  const city = itemCity;
  const state = itemState;
  return (
    <>
      <div className="py-8">
        <ScrollToTop />
        <NavToPages></NavToPages>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div className="container">
            <div className="row">
              <div className="col-sm-6">
                <div className="card my-5">
                  <img src={itemImage} className="card-img-top" alt="..." />
                  <div className="card-body">
                    <h5>Description</h5>
                    <p className="card-text">{itemDescription}</p>
                  </div>
                </div>
              </div>
              <div className="col-sm-5">
                <div className="card my-5">
                  <div className="card-body">
                    <h5 className="card-title">{itemTitle}</h5>
                    <h4 className="card-text">${itemPrice}</h4>
                    <p className="card-text">
                      <small className="text-muted">
                        Posted {itemCreatedAt}
                      </small>
                    </p>
                    <h5 className="card-title">
                      {itemCity}, {itemState}
                    </h5>
                    <p>Condition: {itemCondition}</p>
                    <p>Size: {itemSize}</p>
                    <p>Color: {itemColor}</p>
                    <p>Retail price: ${itemMsrp}</p>
                    <div className="d-grid gap-2">
                      {currentUser ? (
                        <>
                          <button
                            className="btn btn-outline-dark"
                            type="button"
                            data-bs-toggle="modal"
                            data-bs-target="#offerModal"
                          >
                            Make offer
                          </button>
                        </>
                      ) : (
                        <button
                          className="btn btn-outline-dark disabled"
                          type="button"
                          data-bs-toggle="modal"
                          data-bs-target="#offerModal"
                        >
                          login to make an offer
                        </button>
                      )}

                      <div
                        className="modal fade"
                        id="offerModal"
                        tabIndex="-1"
                        aria-labelledby="offerModalLabel"
                        aria-hidden="true"
                      >
                        <div className="modal-dialog">
                          <div className="modal-content">
                            <div className="modal-header">
                              <h5 className="modal-title" id="offerModalLabel">
                                Make An Offer
                              </h5>
                              <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                              ></button>
                            </div>

                            <form onSubmit={handleOffer}>
                              <div className="modal-body">
                                <img
                                  src={itemImage}
                                  className="card-img-top"
                                  alt="..."
                                />
                                <p>Asking:${itemPrice}</p>
                                <div className="form-floating mb-3">
                                  <label>Your offer</label>
                                  <input
                                    placeholder="Input Offer"
                                    onChange={handleChangeOffer}
                                    value={offerPrice}
                                    required
                                    type="number"
                                    name="price"
                                    id="price"
                                    className="form-control"
                                  />
                                  <label htmlFor="price"></label>
                                </div>
                              </div>
                              <div className="modal-footer">
                                <button
                                  type="button"
                                  className="btn btn-outline-danger"
                                  data-bs-dismiss="modal"
                                >
                                  Cancel
                                </button>
                                <button
                                  type="submit"
                                  className="btn btn-outline-success"
                                  data-bs-dismiss="modal"
                                >
                                  Send Offer
                                </button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Weather city={city} state={state} />
                  <div className="container text-center">
                    <Iframe city={city} state={state} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ItemsListOne;
