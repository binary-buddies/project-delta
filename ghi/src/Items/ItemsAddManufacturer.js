import React from "react";
import { NavLink } from "react-router-dom";

class ItemsAddManufacturer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: "",
    };
    this.handleManufacturer = this.handleManufacturer.bind(this);
    this.handleSubmitManufacturer = this.handleSubmitManufacturer.bind(this);
  }
  handleManufacturer(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }
  async handleSubmitManufacturer(event) {
    event.preventDefault();
    const data = { name: this.state.manufacturer };

    const locationUrl = `${process.env.REACT_APP_INVENTORY_API}/api/manufacturers/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      
      const cleared = {
        manufacturer: "",
      };
      this.setState(cleared);
    } else {
      const span = document.getElementById("warning");
      const classes = span.classList;
      classes.remove("d-none");
    }
  }

  render() {
    return (
      <>
        <div className="py-8">
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1 className="text-center"> Add A Manufacturer/Brand</h1>
                <p className="text-center">
                  <NavLink to="/Admin" className="nav-link">
                    Back To Admin Home
                  </NavLink>
                </p>
                <form
                  onSubmit={this.handleSubmitManufacturer}
                  id="add-inventory-form"
                >
                  <div className="form-floating mb-3">
                    <li className="nav-item1"></li>
                    <input
                      onChange={this.handleManufacturer}
                      placeholder="manufacturer"
                      required
                      type="text"
                      name="manufacturer"
                      id="manufacturer"
                      className="form-control"
                      value={this.state.manufacturer}
                    />
                    <label htmlFor="manufacturer">Manufacturer/Brand</label>
                  </div>
                  <button className="btn btn-secondary btn-sm">Create</button>
                </form>
              </div>
            </div>
          </div>
          <div
            className="alert alert-danger alert-dismissible fade show  d-none"
            id="warning"
          >
            <strong>Error!</strong> SOMETHING WENT WRONG!
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="alert"
            ></button>
          </div>
        </div>
      </>
    );
  }
}

export default ItemsAddManufacturer;
