import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FloatingLabel from "react-bootstrap/FloatingLabel";

function ItemForm(props) {
  const { item } = props;
  const navigate = useNavigate();

  const BASE_URL = `${process.env.REACT_APP_INVENTORY_API}`;
  const ITEMS_URL = `${BASE_URL}/api/item_beta/`;
  const CATEGORIES_URL = `${BASE_URL}/api/categories/`;
  const ACTIVITIES_URL = `${BASE_URL}/api/activities/`;
  const MANUFACTURERS_URL = `${BASE_URL}/api/manufacturers/`;

  const [categories, setCategories] = useState([]);
  const [activities, setActivities] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const conditions = [
    "New",
    "Reconditioned",
    "Open box (never used)",
    "Used (normal wear)",
    "For parts",
    "Other (see description)",
  ];

  const [itemTitle, setCurrentItemTitle] = useState();
  const [itemCondition, setCurrentItemCondition] = useState();
  const [itemCategory, setCurrentItemCategory] = useState();
  const [itemActivity, setCurrentItemActivity] = useState();
  const [itemManufacturer, setCurrentItemManufacturer] = useState();
  const [itemColor, setItemColor] = useState();
  const [itemSize, setItemSize] = useState();
  const [itemDescription, setCurrentItemDescription] = useState();
  const [itemMsrp, setCurrentItemMsrp] = useState();
  const [itemPrice, setCurrentItemPrice] = useState();

  // const [itemImage, setCurrentItemImage] = useState();
  // function handleChangeImage(event) {
  //   const value = event.target.files[0];
  //   setCurrentItemImage(value);
  // }

  function handleChangeTitle(event) {
    const value = event.target.value;
    setCurrentItemTitle(value);
  }
  function handleChangeCondition(event) {
    const value = event.target.value;
    setCurrentItemCondition(value);
  }
  function handleChangeCategory(event) {
    const value = event.target.value;
    setCurrentItemCategory(value);
  }
  function handleChangeActivity(event) {
    const value = event.target.value;
    setCurrentItemActivity(value);
  }
  function handleChangeManufacturer(event) {
    const value = event.target.value;
    setCurrentItemManufacturer(value);
  }
  function handleChangeColor(event) {
    const value = event.target.value;
    setItemColor(value);
  }
  function handleChangeSize(event) {
    const value = event.target.value;
    setItemSize(value);
  }
  function handleChangeDescription(event) {
    const value = event.target.value;
    setCurrentItemDescription(value);
  }
  function handleChangeMsrp(event) {
    const value = event.target.value;
    setCurrentItemMsrp(value);
  }
  function handleChangePrice(event) {
    const value = event.target.value;
    setCurrentItemPrice(value);
  }
  async function handleUpdate(event) {
    event.preventDefault();

    const data = {
      title: itemTitle,
      condition: itemCondition,
      category: itemCategory,
      activity: itemActivity,
      manufacturer: itemManufacturer,
      color: itemColor,
      size: itemSize,
      description: itemDescription,
      msrp: itemMsrp,
      price: itemPrice,
    };
    const url = `${ITEMS_URL}${item.id}/`;

    const fetchConfig = {
      method: "put",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(url, fetchConfig);
    navigate(-1);
  }

  useEffect(() => {
    const categoriesUrl = `${CATEGORIES_URL}`;
    fetch(categoriesUrl)
      .then((res) => res.json())
      .then((data) => {
        setCategories(data.categories);
      })
      .catch((error) => {
        console.error("error", error);
      });
  }, [CATEGORIES_URL]);

  useEffect(() => {
    const activitiesUrl = `${ACTIVITIES_URL}`;
    fetch(activitiesUrl)
      .then((res) => res.json())
      .then((data) => {
        setActivities(data.activities);
      })
      .catch((error) => {
        console.error("error", error);
      });
  }, [ACTIVITIES_URL]);

  useEffect(() => {
    const manufacturersUrl = `${MANUFACTURERS_URL}`;
    fetch(manufacturersUrl)
      .then((res) => res.json())
      .then((data) => {
        setManufacturers(data.brands);
      })
      .catch((error) => {
        console.error("error", error);
      });
  }, [MANUFACTURERS_URL]);

  if (item) {
    return (
      <>
        <Form onSubmit={handleUpdate}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <FloatingLabel
              controlId="floatingInput"
              label="Title"
              className="mb-3"
            >
              <Form.Control
                type="text"
                name="title"
                onChange={handleChangeTitle}
                defaultValue={item.title}
              />
            </FloatingLabel>

            <FloatingLabel
              controlId="floatingInput"
              label="Category"
              className="mb-3"
            >
              <Form.Select
                type="text"
                onChange={handleChangeCategory}
                defaultValue={item.category}
              >
                {categories.map((category) => {
                  return (
                    <option key={category.id} value={category.id}>
                      {category.name}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>

            <FloatingLabel
              controlId="floatingInput"
              label="Activity"
              className="mb-3"
            >
              <Form.Select
                type="text"
                onChange={handleChangeActivity}
                defaultValue={item.activity}
              >
                {activities.map((activity) => {
                  return (
                    <option key={activity.id} value={activity.id}>
                      {activity.name}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>

            <FloatingLabel
              controlId="floatingInput"
              label="Manufacturer"
              className="mb-3"
            >
              <Form.Select
                type="text"
                onChange={handleChangeManufacturer}
                defaultValue={item.manufacturer}
              >
                {manufacturers.map((manufacturer) => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>

            <FloatingLabel
              controlId="floatingInput"
              label="Condition"
              className="mb-3"
            >
              <Form.Select
                type="text"
                onChange={handleChangeCondition}
                defaultValue={item.condition}
              >
                {conditions.map((condition) => {
                  return (
                    <option key={condition} value={condition}>
                      {condition}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>

            <FloatingLabel
              controlId="floatingInput"
              label="Color"
              className="mb-3"
            >
              <Form.Control
                type="text"
                onChange={handleChangeColor}
                defaultValue={item.color}
              />
            </FloatingLabel>
            <FloatingLabel
              controlId="floatingInput"
              label="Size"
              className="mb-3"
            >
              <Form.Control
                type="text"
                onChange={handleChangeSize}
                defaultValue={item.size}
              />
            </FloatingLabel>
            <FloatingLabel
              controlId="floatingInput"
              label="Description"
              className="mb-3"
            >
              <Form.Control
                type="areaText"
                onChange={handleChangeDescription}
                defaultValue={item.description}
              />
            </FloatingLabel>
            <FloatingLabel
              controlId="floatingInput"
              label="Manufacturer Suggested Retail Price"
              className="mb-3"
            >
              <Form.Control type="decimal" onChange={handleChangeMsrp} defaultValue={item.msrp} />
            </FloatingLabel>
            <FloatingLabel
              controlId="floatingInput"
              label="List price"
              className="mb-3"
            >
              <Form.Control type="decimal" onChange={handleChangePrice} defaultValue={item.price} />
            </FloatingLabel>

            {/* <FloatingLabel
              controlId="floatingInput"
              label="Image"
              className="mb-3"
            >
              <Form.Control type="file" onChange={handleChangeImage} defaultValue={item.image} />
            </FloatingLabel> */}
          </Form.Group>
          <Button variant="primary" type="submit">
            Update
          </Button>
        </Form>
      </>
    );
  }
}

export default function UpdateItem(props) {
  const { item_id } = useParams();
  const BASE_URL = `${process.env.REACT_APP_INVENTORY_API}/api/item_beta/`;
  const [item, setCurrentItem] = useState([]);

  useEffect(() => {
    const itemUrl = `${BASE_URL}${item_id}/`;
    fetch(itemUrl)
      .then((res) => res.json())
      .then((data) => {
        setCurrentItem(data);
      });
  }, [item_id, BASE_URL]);

  return (
    <>
      <div className="py-7">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Edit your sale item</h1>
              <ItemForm item={item}></ItemForm>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
