import React from "react";
import { Link } from "react-router-dom";
// import ScrollToTop from "react-scroll-to-top";


function ItemColumn(props) {
  return (
    <>
      <div className="col" id="inventoryList">
        {props.list.map((item) => {
          return (
            <div
              key={item.id}
              id={`item_${item.id}`}
              className="card mb-3 shadow"
            >
              <Link
                to={`/items/${item.id}`}
                style={{ textDecoration: "none", color: "black" }}
              >
                {item.image && (
                  <img
                    src={item.image}
                    className="card-img-top"
                    alt=""
                  />
                )}
                <div className="card-body">
                  <h5 className="card-title">{item.title}</h5>
                  <h6 className="card-title">{item.manufacturer.name}</h6>
                  <p>
                    {item.city}, {item.state}
                  </p>
                </div>
                <div className="card-footer">Price: ${item.price}</div>
              </Link>
            </div>
          );
        })}
      </div>
    </>
  );
}

class InventoryList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemsToShow: [],
      manufacturer: undefined,
      manufacturers: [],
    };
    this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
  }
  async componentDidMount() {
    this.manufacturerCard();
  }

  async manufacturerCard() {
    {
      const manufacturerUrl = `${process.env.REACT_APP_INVENTORY_API}/api/manufacturers/`;

      const response = await fetch(manufacturerUrl);

      if (response.ok) {
        const data = await response.json();
        this.setState({ manufacturers: data.brands });
      }
    }

    {
      const url = `${process.env.REACT_APP_INVENTORY_API}/api/items/available/`;
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          const requests = [];
          for (let item of data.items) {
            const detailUrl = `${process.env.REACT_APP_INVENTORY_API}/api/items/${item.id}/`;
            requests.push(fetch(detailUrl));
          }
          const responses = await Promise.all(requests);

          let itemsToShow = [];

          for (const itemResponse of responses) {
            if (itemResponse.ok) {
              const details = await itemResponse.json();
              if (
                this.state.manufacturer === undefined ||
                this.state.manufacturer === ""
              ) {
                itemsToShow.push(details);
              } else {
                if (
                  parseInt(this.state.manufacturer) ===
                  parseInt(details.manufacturer.id)
                ) {
                  itemsToShow.push(details);
                }
              }
            } else {
              console.error(itemResponse);
            }
          }
          this.setState({ itemsToShow: itemsToShow });
        }
      } catch (e) {
        console.error(e);
      }
    }
  }
  handleChangeManufacturer(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value }, () => {
      this.manufacturerCard();
    });
  }

  render() {
    const itemsToShow = this.state.itemsToShow.filter(
      (item) =>
        item.status === "Approved" &&
        (!this.props.searchTerm ||
          item.title.toLowerCase().includes(this.props.searchTerm))
    );
    const oneThird = Math.round(itemsToShow.length / 3);
    const itemColumns = [
      itemsToShow.slice(0, oneThird),
      itemsToShow.slice(oneThird, 2 * oneThird),
      itemsToShow.slice(2 * oneThird),
    ];
    itemColumns.reverse();
    return (
      <>
        <div className="py-8">
          {/* <ScrollToTop smooth></ScrollToTop> */}
          <table className="table table-striped"></table>
          <div className="input-group1  mx-auto mb-3">
            <select
              onChange={this.handleChangeManufacturer}
              required
              name="manufacturer"
              id="manufacturer"
              className="form-select"
            >
              <option value="">Sort by Manufacturers</option>
              {this.state.manufacturers.map((manufacturer) => {
                return (
                  <option key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                  </option>
                );
              })}
            </select>
          </div>

          <div className="container">
            <h2 className="text-center">All Gear</h2>
            <div className="row">
              {itemColumns.map((InventoryList, index) => {
                return <ItemColumn key={index} list={InventoryList} />;
              })}
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default InventoryList;
