import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

function SearchBar(props) {
  const [search, setSearch] = useState("");

  const navigate = useNavigate();
  const searchOnClick = () => {
    props.setSearchTerm(search.toLowerCase());
    setSearch("");
    navigate("/SearchResults");
  };
  return (
    <>
      <div className="SearchNav">
        <input
          onChange={(e) => setSearch(e.target.value)}
          type="text"
          placeholder="Search..."
          value={search}
        />
        <button
          onClick={searchOnClick}
          className="btn btn-sm  bg-gradient-secondary  mb-1 me-0"
        >
          Search
        </button>
      </div>
    </>
  );
}
export default SearchBar;
