import React from "react";
import { NavLink } from "react-router-dom";

class CategoriesAdd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: "",
    };
    this.handleCategory = this.handleCategory.bind(this);
    this.handleSubmitCategory = this.handleSubmitCategory.bind(this);
  }
  handleCategory(event) {
    const value = event.target.value;
    this.setState({ category: value });
  }
  async handleSubmitCategory(event) {
    event.preventDefault();
    const data = { name: this.state.category };

    const locationUrl = `${process.env.REACT_APP_INVENTORY_API}/api/categories/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      const cleared = {
        category: "",
      };
      this.setState(cleared);
    } else {
      const span = document.getElementById("warning");
      const classes = span.classList;
      classes.remove("d-none");
    }
  }

  render() {
    return (
      <>
        <div className="py-8">
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1 className="text-center"> Add A Category</h1>
                <p className="text-center">
                  <NavLink to="/Admin" className="nav-link">
                    Back To Admin Home
                  </NavLink>
                </p>

                <form
                  onSubmit={this.handleSubmitCategory}
                  id="add-inventory-form"
                >
                  <div className="form-floating mb-3">
                    <input
                      onChange={this.handleCategory}
                      placeholder="category"
                      required
                      type="text"
                      name="category"
                      id="category"
                      className="form-control"
                      value={this.state.category}
                    />
                    <label htmlFor="category">Category</label>
                  </div>
                  <button className="btn btn-secondary btn-sm">Create</button>
                </form>
              </div>
            </div>
          </div>
          <div
            className="alert alert-danger alert-dismissible fade show  d-none"
            id="warning"
          >
            <strong>Error!</strong> SOMETHING WENT WRONG!
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="alert"
            ></button>
          </div>
        </div>
      </>
    );
  }
}
export default CategoriesAdd;
