import React from "react";
import ItemColumn from "../ItemColumn/ItemColumn";
import NavToPages from "../NavToPages";

class CategoriesSki extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemColumns: [[], [], []],
      manufacturer: undefined,
      manufacturers: [],
    };
    this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
  }
  async componentDidMount() {
    this.manufacturerCard();
  }

  async manufacturerCard() {
    {
      const manufacturerUrl = `${process.env.REACT_APP_INVENTORY_API}/api/manufacturers/`;
      const response = await fetch(manufacturerUrl);
      if (response.ok) {
        const data = await response.json();
        this.setState({ manufacturers: data.brands });
      }
    }
    {
      const url = `${process.env.REACT_APP_INVENTORY_API}/api/items/available/`;
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          const requests = [];
          for (let item of data.items) {
            const detailUrl = `${process.env.REACT_APP_INVENTORY_API}/api/items/${item.id}/`;
            requests.push(fetch(detailUrl));
          }

          const responses = await Promise.all(requests);
          // remove columns

          // added let itemsTo Show and moved the itemColumns to the bottom
          let itemsToShow = [];

          for (const itemResponse of responses) {
            if (itemResponse.ok) {
              const details = await itemResponse.json();
              if (
                this.state.manufacturer === undefined ||
                this.state.manufacturer === ""
              ) {
                // removed the math and added to push the items
                itemsToShow.push(details);
              } else {
                if (
                  parseInt(this.state.manufacturer) ===
                  parseInt(details.manufacturer.id)
                ) {
                  // removed the math and added to push the items
                  itemsToShow.push(details);
                }
              }
            } else {
              console.error(itemResponse);
            }
          }
          // the next three lines were added to handle the way the cards filled into the columns
          // the next line of code for filter is on category pages because of the == ski, it will only show ski. Change that to what category you are on
          itemsToShow = itemsToShow.filter(
            (item) => item.activity.name === "Skiing/Snowboarding"
          );
          const oneThird = Math.round(itemsToShow.length / 3);
          const itemColumns = [
            itemsToShow.slice(0, oneThird),
            itemsToShow.slice(oneThird, 2 * oneThird),
            itemsToShow.slice(2 * oneThird),
          ];
          itemColumns.reverse();
          this.setState({ itemColumns: itemColumns });
        }
      } catch (e) {
        console.error(e);
      }
    }
  }
  handleChangeManufacturer(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value }, () => {
      this.manufacturerCard();
    });
  }

  render() {
    return (
      <>
        <div className="py-8">
          <NavToPages></NavToPages>
          <table className="table table-striped"></table>
          <div className="input-group1  mx-auto mb-3">
            <select
              onChange={this.handleChangeManufacturer}
              required
              name="manufacturer"
              id="manufacturer"
              className="form-select"
            >
              <option value="">Sort by Manufacturers</option>
              {this.state.manufacturers.map((manufacturer) => {
                return (
                  <option key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="container">
            <h2 className="text-center">Skiing and Snowboarding</h2>
            <div className="row">
              {this.state.itemColumns.map((SkiList, index) => {
                return (
                  <ItemColumn
                    key={index}
                    list={SkiList}
                    activity="Skiing/Snowboarding"
                  />
                );
              })}
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default CategoriesSki;
