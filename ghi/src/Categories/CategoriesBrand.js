import React from "react";

const size = 4;
function CategoriesBrands(props) {
  const array = props.brands.brands;


  const grid = [];
  for (let i = 0; i < array.length; i += size) {
    const chunk = array.slice(i, i + size);
    grid.push(chunk);
  }
  return (
    <>
      <h1 className="text-center"> Brands</h1>
      <div className="mx-auto w-75">
        <table className="manufactureTable ">
          <tbody>
            {grid.map((subGrid, index) => {
              return (
                <tr
                  className="manufactureTable "
                  key={subGrid.length + "_" + index}
                >
                  {subGrid.map((manufacturer, index) => {
                    return (
                      <td key={manufacturer.id + "-" + index}>
                        <div className="card-body card mb-3 shadow">
                          <h5 className="card-title">{manufacturer.name}</h5>
                        </div>
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default CategoriesBrands;
