import React from "react";
import InventoryList from "./Items/ItemsListAll";

function SearchResults(props) {
  return <InventoryList searchTerm={props.searchTerm} />;
}

export default SearchResults;
