import React from "react";

function ItemColumn(props) {
  return (
    <div className="col">
      {props.list
        .filter((item) => item.activity.name === props.activity)
        .map((item) => {
          return (
            <div
              key={item.id}
              id={`item_${item.id}`}
              className="card mb-3 shadow"
            >
              {item.image && (
                <a href={`items/${item.id}`}>
                  <img
                    src={item.image}
                    className="card-img-top"
                    alt=""
                  />
                </a>
              )}
              <div className="card-body">
                <h5 className="card-title">{item.title}</h5>
                <p className="card-title">{item.activity.name}</p>
                <p className="card-title">{item.manufacturer.name}</p>
              </div>
              <div className="card-footer">Price: ${item.price}</div>
            </div>
          );
        })}
    </div>
  );
}
export default ItemColumn;
