import React from "react";
import HeroPic from "./Images/Main-Pic-1-Resized.png";
import InventoryList from "./Items/ItemsListAll";
import NavToPages from "./NavToPages";

function MainPage(props) {
  return (
    <>
      <div>
        <div
          className="page-header min-vh-90"
          style={{ backgroundImage: "url(" + HeroPic + ")" }}
          loading="lazy"
        >
          <span className="mask bg-gradient-dark opacity-4"></span>
        </div>
      </div>
      <NavToPages></NavToPages>
      <div>
        <InventoryList searchTerm={props.searchTerm} />
      </div>
    </>
  );
}

export default MainPage;
