import { Routes, Route, useLocation, Navigate } from "react-router-dom";
import "./App.css";
import Nav from "./Nav";
import MainPage from "./Main";
import Chat from "./Chat";
import { useToken } from "./authApi";
import { useState, useEffect } from "react";

import CategoriesAdd from "./Categories/CategoriesAdd";
import CategoriesClimb from "./Categories/CategoriesClimb";
import CategoriesCamp from "./Categories/CategoriesCamp";
import CategoriesClothing from "./Categories/CategoriesClothing";
import CategoriesSki from "./Categories/CategoriesSki";
import CategoriesBrand from "./Categories/CategoriesBrand";

import InventoryForm from './Items/ItemsAdd';
import ItemsListAll from './Items/ItemsListAll';
import ItemsListOne from './Items/ItemsListOne';
import UpdateItem from './Items/UpdateItem';
import ItemsAddManufacturer from './Items/ItemsAddManufacturer';

import UserDashboardBeta from './User/UserDashboard/UserDashboardBeta';
import UserLogin from './User/UserLogin';
import UserSignup from './User/UserSignup';
import UserEdit from './User/UserEdit';
import Logout from './User/Logout';

import Admin from './Admin/Admin';
import Submitted from './Admin/Submitted';
import RejectedPost from './Admin/RejectedPost';
import ApprovedPost from './Admin/ApprovedPost';
import AdminAdmin from './Admin/AdminAdmin';

import Weather from './MapWeather/Weather';
import SearchResults from './SearchResults';
import ActivitiesAdd from './Activities/ActivitiesAdd';

function App(props) {
  const [token, login, logout, signup, user] = useToken();
  const [searchTerm, setSearchTerm] = useState("");
  const location = useLocation();
  useEffect(() => {
    if (location.pathname !== "/SearchResults") {
      setSearchTerm("");
    }
  }, [location]);
  // the above is used to clear the state of the searchBar after a page change(location)
  return (
    <div>
      <Nav token={token} user={user} setSearchTerm={setSearchTerm} />
      <Routes>
        <Route path="/" element={<MainPage searchTerm={searchTerm} />} />
        <Route path="/chat" element={<Chat token={token} />} />
        <Route path="/CategoriesAdd" element={<CategoriesAdd />} />
        <Route path="/CategoriesClimb" element={<CategoriesClimb />} />
        <Route path="/CategoriesSki" element={<CategoriesSki />} />
        <Route path="/CategoriesCamp" element={<CategoriesCamp />} />
        <Route path="/CategoriesClothing" element={<CategoriesClothing />} />
        <Route
          path="/CategoriesBrand"
          element={<CategoriesBrand brands={props.brands} />}
        />

        <Route path="/ActivitiesAdd" element={<ActivitiesAdd />} />

        <Route
          path="/ItemsAddManufacturer"
          element={<ItemsAddManufacturer />}
        />

        <Route path="items">
          <Route
            path=""
            element={
              <ItemsListAll
                items={props.items}
                token={token}
                searchTerm={searchTerm}
              />
            }
          />
          <Route path="add" element={<InventoryForm user={user} />} />
          <Route path=":itemId" element={<ItemsListOne user={user} />} />
          <Route path="update/:item_id" element={<UpdateItem user={user} />} />
        </Route>

        <Route
          path="/UserDashboardBeta"
          element={<UserDashboardBeta user={user} />}
        />
        <Route
          path="/UserLogin"
          element={<UserLogin token={token} login={login} />}
        />
        <Route
          path="/UserSignup"
          element={<UserSignup token={token} signup={signup} />}
        />
        <Route path="/UserEdit" element={<UserEdit user={user} />} />
        <Route path="/Logout" element={<Logout logout={logout} />} />

        <Route path="/Admin" element={<Admin user={user} />} />
        <Route path="/AdminAdmin" element={<AdminAdmin user={user} />} />

        <Route path="/Submitted" element={<Submitted />} />
        <Route path="/RejectedPost" element={<RejectedPost />} />
        <Route path="/ApprovedPost" element={<ApprovedPost />} />

        <Route path="/Weather" element={<Weather user={user} />} />

        <Route
          path="/SearchResults"
          element={<SearchResults searchTerm={searchTerm} />}
        />

        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </div>
  );
}

export default App;
