import { NavLink } from "react-router-dom";

export default function NavToPages(props) {
  return (
    <>
      <nav
        className="navbar navbar-expand-md navbar-light bg-light bg-gradient bg-opacity-25"
        id="nav2"
      >
        <div className="mx-auto">
          <ul className="cat-nav container">
            <NavLink className="navbar-brand2" to="/CategoriesClimb">
              Climbing
            </NavLink>
            |
            <NavLink className="navbar-brand2" to="/CategoriesSki">
              Ski/Snowboarding
            </NavLink>
            |
            <NavLink className="navbar-brand2" to="/CategoriesCamp">
              Hiking/Backpacking
            </NavLink>
            |
            <NavLink className="navbar-brand2" to="/CategoriesClothing">
              Clothing
            </NavLink>
            |
            <NavLink className="navbar-brand2" to="/items">
              All gear
            </NavLink>
          </ul>
        </div>
      </nav>
    </>
  );
}
