import React from "react";
import { NavLink } from "react-router-dom";

class ActivitiesAdd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activity: "",
    };
    this.handleActivity = this.handleActivity.bind(this);
    this.handleSubmitActivity = this.handleSubmitActivity.bind(this);
  }
  handleActivity(event) {
    const value = event.target.value;
    this.setState({ activity: value });
  }
  async handleSubmitActivity(event) {
    event.preventDefault();
    const data = { name: this.state.activity };

    const locationUrl = `${process.env.REACT_APP_INVENTORY_API}/api/activities/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      

      const cleared = {
        activity: "",
      };
      this.setState(cleared);
    } else {
      const span = document.getElementById("warning");
      const classes = span.classList;
      classes.remove("d-none");
    }
  }

  render() {
    return (
      <>
        <div className="py-8">
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1 className="text-center"> Add An Activity</h1>
                <p className="text-center">
                  <NavLink to="/Admin" className="nav-link">
                    Back To Admin Home
                  </NavLink>
                </p>

                <form
                  onSubmit={this.handleSubmitActivity}
                  id="add-inventory-form"
                >
                  <div className="form-floating mb-3">
                    <input
                      onChange={this.handleActivity}
                      placeholder="activity"
                      required
                      type="text"
                      name="activity"
                      id="activity"
                      className="form-control"
                      value={this.state.activity}
                    />
                    <label htmlFor="activity">Activity</label>
                  </div>
                  <button className="btn btn-secondary btn-sm">Create</button>
                </form>
              </div>
            </div>
          </div>
          <div
            className="alert alert-danger alert-dismissible fade show  d-none"
            id="warning"
          >
            <strong>Error!</strong> SOMETHING WENT WRONG!
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="alert"
            ></button>
          </div>
        </div>
      </>
    );
  }
}
export default ActivitiesAdd;
