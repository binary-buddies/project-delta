# project-delta

## Design

* [API design](docs/apis.md)
* [Data model](docs/data-model.md)
* [GHI](docs/ghi.md)
* [Integrations](docs/integrations.md)

## Name of application
- Isella Outdoor Consignment

## Team members
- Amanda V.
- Antonio
- Jessica
- Matty

## Summary
Allows users to buy and sell second-hand outdoor gear.

## Market
Women and non-binary people interested in pursuing technical outdoor sports looking for gear at reasonable prices.

## Features
- Users can create items available for sale either only locally/only shipping or locally AND they are willing to ship
- Users can create and manage items for sale (including uploading up to N images)
- Users can report inappropriate content
- Users can message sellers to ask questions about gear
- The app can calculate shipping based on USPS API for items that need to be shipped to buyer
- App will have a radius map on all postings which will indicate where the seller is located
- Users can filter by category and/or do a keyword search
- App can process payments for sales
- App will have a free section for donations
- If item doesn't sell after a certain number of days, price is decreased by a percentage
- Buyers can "make an offer" to sellers with their suggested price
- App will have a blog
- App will have a section to post "in search of" for people looking for specific items
- Users can post events which will be displayed on "events" page
- Users can sign up for notifications for specific items (i.e. "Blue patagonia jacket")