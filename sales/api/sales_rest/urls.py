from django.urls import path
from .views import (
    offers_list,
    offer_list,
    api_offers_by_seller_email,
    api_offers_by_buyer_email,
    api_pending_offers_by_buyer_email,
    api_final_offers_by_buyer_email,
    api_offers_by_item_id,
    api_offer_accepted,
    api_offer_unaccepted,
    api_mark_offer_final_sale,
)

urlpatterns = [
    path("offers/", offers_list, name="offers"),
    path("offers/<int:pk>/", offer_list, name="offer"),
    path(
        "offers/seller/<str:email>/",
        api_offers_by_seller_email,
        name="offers_by_seller_email",
    ),
    path(
        "offers/buyer/<str:email>/",
        api_offers_by_buyer_email,
        name="offers_by_buyer_email",
    ),
    path(
        "offers/buyer/pending_offers/<str:email>/",
        api_pending_offers_by_buyer_email,
        name="api_pending_offers_by_buyer_email",
    ),
    path(
        "offers/buyer/final_offers/<str:email>/",
        api_final_offers_by_buyer_email,
        name="api_final_offers_by_buyer_email",
    ),
    path(
        "offers/item/<int:item_id>/",
        api_offers_by_item_id,
        name="api_offers_by_item_id",
    ),
    path(
        "offers/accepted/<int:offer_id>/", api_offer_accepted, name="api_offer_accepted"
    ),
    path(
        "offers/unaccepted/<int:offer_id>/",
        api_offer_unaccepted,
        name="api_offer_unaccepted",
    ),
    path(
        "offers/sold/<int:offer_id>/",
        api_mark_offer_final_sale,
        name="api_mark_offer_final_sale",
    ),
]
