from common.json import ModelEncoder
from .models import Offer, ItemVO


class ItemVOEncoder(ModelEncoder):
    model = ItemVO
    properties = ["id", "title", "price", "image"]


class OfferCreateEncoder(ModelEncoder):
    model = Offer
    properties = [
        "id",
        "seller",
        "buyer",
        "item",
        "price",
    ]
    encoders = {
        "item": ItemVOEncoder(),
    }


class OfferEncoder(ModelEncoder):
    model = Offer
    properties = [
        "id",
        "seller",
        "buyer",
        "item",
        "created_at",
        "accepted_at",
        "price",
        "in_person",
        "final_accepted_offer",
        "archive",
    ]
    encoders = {
        "item": ItemVOEncoder(),
    }
