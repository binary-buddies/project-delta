from django.test import TestCase, Client
import json


class ApiOffersTests(TestCase):
    @classmethod
    def setUp(self):
        self.client = Client()

    def test_get_offers(self):
        response = self.client.get("/api/offers/")
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn("offers", content)

    # def test_post_offer(self):
    #     activity = json.dumps(
    #         {
    #             "seller": "seller@test.com",
    #             "buyer": "buyer@test.com",
    #             "item": 895,
    #             "price": 20.0,
    #         }
    #     )
    #     response = self.client.post("/api/offers/", activity, "application/json")
    #     content = json.loads(response.content)
    #     self.assertEqual(response.status_code, 200)
    #     self.assertIn("seller", content)
