import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Offer, ItemVO
from .encoders import OfferEncoder, OfferCreateEncoder
import pika

four_zero_four_error = {"response": "object does not exist"}


@require_http_methods(["GET"])
def api_offers_by_seller_email(request, email):
    try:
        offers = Offer.objects.filter(seller=email)
        return JsonResponse({"offers": offers}, encoder=OfferEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Error"})
        response.status_code = 404
        return response


@require_http_methods(["GET"])
def api_offers_by_buyer_email(request, email):
    try:
        offers = Offer.objects.filter(
            buyer=email,
            final_accepted_offer=False,
            accepted_at__isnull=True,
            archive=False,
        )
        return JsonResponse({"offers": offers}, encoder=OfferEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Error"})
        response.status_code = 404
        return response


@require_http_methods(["GET"])
def api_pending_offers_by_buyer_email(request, email):
    try:
        offers = Offer.objects.filter(
            buyer=email,
            final_accepted_offer=False,
            accepted_at__isnull=False,
            archive=False,
        )
        return JsonResponse({"offers": offers}, encoder=OfferEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Error"})
        response.status_code = 404
        return response


@require_http_methods(["GET"])
def api_final_offers_by_buyer_email(request, email):
    try:
        offers = Offer.objects.filter(
            buyer=email,
            final_accepted_offer=True,
        )
        return JsonResponse({"offers": offers}, encoder=OfferEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Error"})
        response.status_code = 404
        return response


@require_http_methods(["GET"])
def api_offers_by_item_id(request, item_id):
    try:
        offers = Offer.objects.filter(item=item_id)
        return JsonResponse({"offers": offers}, encoder=OfferEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Error"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_offer_accepted(request, offer_id):
    try:
        offer = Offer.objects.get(id=offer_id)
        offer.offer_accepted()
        return JsonResponse({"offer": offer}, encoder=OfferEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Error"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_offer_unaccepted(request, offer_id):
    try:
        offer = Offer.objects.get(id=offer_id)
        offer.offer_unaccepted()
        return JsonResponse({"offer": offer}, encoder=OfferEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Error"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_mark_offer_final_sale(request, offer_id):
    try:
        # Upon final sale, mark all offers for specific item as archived
        offer = Offer.objects.get(id=offer_id)
        item_id = offer.item
        all_offers_for_item = Offer.objects.filter(item=item_id)
        print(all_offers_for_item)
        for offer in all_offers_for_item.iterator():
            offer.send_to_archive()
        # Then, mark the correct one as sold and not archived. Then there is one final offer associated with item.
        offer.set_sale_final()
        offer.unarchive()
        return JsonResponse({"offer": offer}, encoder=OfferEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Error"})
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def offers_list(request):
    if request.method == "GET":
        offers = Offer.objects.all()
        return JsonResponse({"offers": offers}, encoder=OfferEncoder)
    else:
        try:
            content = json.loads(request.body)
            item_id = content["item"]
            try:
                item = ItemVO.objects.get(id=item_id)
                content["item"] = item
            except ItemVO.DoesNotExist:
                response = JsonResponse({"message": "Item does not exist"})
                response.status_code = 404
                return response
            content["accepted_at"] = None
            offer = Offer.objects.create(**content)
            offer_price = content["price"]
            item_title = content["item"].title
            seller_email = content["seller"]
            offer_dict = {
                "offer_price": offer_price,
                "item_title": item_title,
                "seller": seller_email,
            }
            send_message(json.dumps(offer_dict), "process_offer_received")
            return JsonResponse(offer, encoder=OfferCreateEncoder, safe=False)
        except Exception:
            response = JsonResponse({"message": "Could not create the offer."})
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT"])
def offer_list(request, pk):

    # GET ONE OFFER
    if request.method == "GET":
        try:
            offer = Offer.objects.get(id=pk)
            return JsonResponse(offer, encoder=OfferEncoder, safe=False)
        except Offer.DoesNotExist:
            response = JsonResponse(four_zero_four_error)
            return response

    # EDIT ONE OFFER
    else:
        try:
            data = json.loads(request.body)
            offer = Offer.objects.get(id=pk)

            props = [
                "seller",
                "buyer",
                "item",
                "created_at",
                "accepted_at",
                "price",
                "in_person",
            ]

            for prop in props:
                if prop in data:
                    setattr(offer, prop, data[prop])
            offer.save()
            return JsonResponse(
                offer,
                encoder=OfferEncoder,
                safe=False,
            )
        except Offer.DoesNotExist:
            response = JsonResponse(four_zero_four_error)
            return response


def send_message(data, queueName):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queueName)
    channel.basic_publish(
        exchange="",
        routing_key=queueName,
        body=data,
    )
    connection.close()
    return
