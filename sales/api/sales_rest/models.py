from django.db import models
import datetime


class ItemVO(models.Model):
    title = models.CharField(max_length=250)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    image = models.TextField()
    seller = models.CharField(max_length=250, null=True)


class Offer(models.Model):
    seller = models.CharField(max_length=200)
    buyer = models.CharField(max_length=100)
    item = models.ForeignKey(ItemVO, related_name="itemVO", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now=True)
    accepted_at = models.DateTimeField(default=False, null=True, blank=True)
    price = models.DecimalField(max_digits=9, decimal_places=2, help_text="all prices are in US Dollars")
    in_person = models.BooleanField(default=False)
    final_accepted_offer = models.BooleanField(default=False, null=True)
    archive = models.BooleanField(default=False)

    def __str__(self):
        return self.seller

    def offer_accepted(self):
        self.accepted_at = datetime.datetime.now()
        self.save()

    def offer_unaccepted(self):
        self.accepted_at = None
        self.save()

    def set_sale_final(self):
        self.final_accepted_offer = True
        self.save()

    def send_to_archive(self):
        self.archive = True
        self.save()

    def unarchive(self):
        self.archive = False
        self.save()
