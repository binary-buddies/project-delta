import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
from sales_rest.models import ItemVO


def get_items():
    base_url = os.environ["SALES_APP_POLLER"]
    response = requests.get(f"{base_url}/api/items/")
    content = json.loads(response.content)
    for item in content["item"]:
        ItemVO.objects.update_or_create(
            id=item["id"],
            defaults={
                "title": item["title"],
                "price": item["price"],
                "image": item["image"],
                "seller": item["seller"],
            },
        )


def poll():
    while True:
        print("Sales poller polling for data")
        try:
            get_items()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
