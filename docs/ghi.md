# Graphical Human Interface

## Main Page Sketch 1 & Main Page Sketch Alt

![Main Page Sketch 1](./wireframes/MainPageSketch1.png)
![Main Page Sketch Alt](./wireframes/MainPageSketchAlt.png)

First Rough Draft: Not Planning to use this format, was orginal ideas.

This would be an option for the main page people see when they go to the website. This is more of an all-inclusive where we're interacting and sorting on the main page 

Key Features for the main page are the top nav bar which includes:

    Nav Bar:
        - hat logo that will be clickable to return to home
        - Search bar to search for items throughout the app
        - Log in / log out button (though user won't have to be logged in/out to browse)
        - shopping cart
    
    Image with name and company brand/mission statement
        - make sure to pick an image and font that allows the text can be seen
    
    Options to choose from
     - More of a single-page interaction sort/feel

## Split-Main-And-Category

![Main AND Category Page](./wireframes/Split-Main-And-Category.png)

Planning to use these ideas

This would be an option for the main page and what a specific categorty would look like. A category would either be a category of women's climbing gear OR  a category of sport (depending on which direction we're going with this).

Key Features for the main page are the top nav bar which includes:

    Nav Bar:
        - hat logo that will be clickable to return to home
        - Search bar to search for items throughout the app
        - Log in / log out button (though user won't have to be logged in/out to browse)
        - shopping cart
    
    Image with name and company brand/mission statement
        - make sure to pick an image and font that allows the text can be seen

    A slide-in visual while scrolling down to see the categories come in. Categories will have sub-categories listed opposite them

    Category picture will be clickable to take you to a page where you can see ISELLA has now changed to the category banner. 

    It would have an option of either a nav bar of options to choose from or a drop-down selection.

    Once you select the sub-category, it would then filter those card items (should show all cards before filtering to capture attention of user, therefore user doesn't show up to a blank page and could see something they didn't even realize they needed)

    Cards will be clickable and take you to a detail page where there will be more functionality


## Detail Page 1

![detail page](./wireframes/Detail-Page-1.png)

When a user clicks on a card in a category, they would be brought to a detail page for that listed item. 

The detail page will consist of a main image (which is also the main listing image) and have all images listed below. When you click on another image, that becomes the large image.

Below the image there is a larger, green, centered "add to cart" button to direct the eye to add it to the cart. There are also smaller sized buttons on each side of it for "message seller" or "make an offer" since these may be steps the buyer wants to take prior to just purchasing, but they have to do with one another therefore they are together. 

The price will be listed on the card in the main list, but it will also appear below the photo, after the add to cart button. To not clutter the page, I don't think the price needs as much emphesis as the person already saw the price when they clicked the card - it is still listed though.

There will be a place for condition, description and location.
    - it is important to note that we will need to add more fields to this decription area suchas manufacturer, size, color etc

I like the idea of the content centered on the page. I played around with putting all the add to cart stuff to the right of the photo, but it made the balance feel really off to me. We can discuss further if the team decides they would prefer a more focused approach. 

I also found the idea of it being centered as an opportunity to have room for ads on the side of the page without having to incoporate ads throughout the context and make it a bad user experience. 


## Account User's Dashboard

![User Dashboard Page](./wireframes/User-Dashboard.png)

The Account user's dashboard will have a navigation bar that, when the item is clicked on, it navgates to that section on the page. Right now, the options are Active listings, Sale History, Purchase history and Messages. Messages can have an indication of a new message. I beleive messages will be it's own microservice and, when selected, it will navigate to another page that contains the messages. At the top of the dashboard page, the user will have a profile picture, their currently set location, a user photo (optional upload), and their seller ratings will be visable. We can choose to add more on there, but I think this is a good area to start. The user can update their information with the edit button on the top right corner. 


## Messaging page

![User Messaging Page](./wireframes/Messages-Page.png)

Since the messaging page will be very busy with messages between users and the account holder, I figured that a simple page that holds a table of messages which could be clicked on and expanded to the side would be nice. I feel like I would like to find prebuilt-options that are currently available for formatting the messages before I can have a better idea of what the messages will look like inside the reserved block for messages.  

## Favorites Page

![User Favorites Page](./wireframes/Favorites-page.png)

We may make a favorites page instead of a cart. If this is the case, this is a simple page layout that is structured like the account dashboard/messaging page


## Color Scheme Vision

My vision here is that the page stays nice and bright white background and black text for dark contrast. There will be a splash of color with the hat logo in the nav bar which brings us home and there will be a deep-colored outdoorsy image that maybe fades lighter on top, which would maybe include mountains, sky and a female with her back to the camera and looking towards her goal of a climb

The reason for the mainly white with black contrast is because there will be so much busyness with the different colors of the posting pictures. Any added color throughout the page would become chaotic and hard on the eyes. I think allowing the images to be the splashes of color given to the page would make the page airy and inviting and would allow the images to pop for the purpose of scrolling and shopping. 


## Notes

events page stretch goal?

where do we want the radius? In user dashboard to edit or in the search page?

How many sort options do we want and what would those look like? (gear manufacturers)(sizes)

on the items detail page, see the location ~~~

keywords in the search 

Look into this for transition of pictures/categories as you scroll:
css & JS for 
transition
When something appears on the screen you want JS to add a class and the class is telling it to transition or to move 
	-transition
	-transform 
	-opacity 0%-100%
	-start -20px off shift to 0px
	-want the transition for the transform and opacity 

