# Integrations

The application will need to get the following kinds of
data from third-party sources:

    - USPS API
      . need user(buyer/seller) city, state to configure shipping rates
    
    - Weather API
      . need city, state to obtain weather details for area
    
    - Google Maps API
      . need user(seller/buyer) city, state to configure longitude/latitude for map to show location of sellers item
 ____
  city, state location can provide longitude/latitude which will be saved data to be used by all APIs above
 ____

    - Stretch Goals API
      . NSFW API to monitor for inappropriate photos
      . Manufactures/Brands API if able to locate one


      test 2