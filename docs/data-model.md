# Data models
## Item :
***
| NAME              |   TYPE   | UNIQUE | OPTIONAL |
| ----------------- | :------: | :----: | :------: |
| id                |   int    |  yes   |    no    |
| _user_id_         |   obj    |   no   |    no    |
| created_at        | datetime |   no   |    no    |
| title             |  string  |   no   |    no    |
| _category_id_     |   obj    |   no   |    no    |
| condition         |  string  |   no   |    no    |
| _manufacturer_id_ |   obj    |   no   |    no    |
| image             |  string  |   no   |   yes    |
| color             |  string  |   no   |   yes    |
| size              |  string  |   no   |    no    |
| msrp              |  float   |   no   |   yes    |
| description       |  string  |   no   |    no    |
| price             |  float   |   no   |    no    |
| sold              | boolean  |   no   |    no    |

## Shipping:
***
| NAME | TYPE | UNIQUE | OPTIONAL |
|--|:--:|:--:|:--:|
| name | string | no | no |
| _item_id_ | obj | yes | no |
| dimensions | array | no | no |
| weight | float(2) | no | no |

## User :
***
| NAME          |  TYPE  | UNIQUE | OPTIONAL |
| ------------- | :----: | :----: | :------: |
| name          | string |   no   |    no    |
| _location_id_ |  obj   |   no   |    no    |
| email         | string |  yes   |    no    |


## Brand :
***
| NAME |  TYPE  | UNIQUE | OPTIONAL |
| ---- | :----: | :----: | :------: |
| name | string |  yes   |    no    |


## Offer :
***
| NAME       |   TYPE   | UNIQUE | OPTIONAL |
| ---------- | :------: | :----: | :------: |
| id         | integer  |  yes   |    no    |
| _seller_   | integer  |   no   |    no    |
| _buyer_    | integer  |   no   |    no    |
| _item_id_  | integer  |  yes   |    no    |
| created_at | timedate |   no   |    no    |
| accepted_at | timedate |   no   |    no    |
| price      |  float   |   no   |    no    |
| in-person  | boolean  |   no   |    no    |


## Location :
***
| NAME         |  TYPE   | UNIQUE | OPTIONAL |
| ------------ | :-----: | :----: | :------: |
| name         | integer |   no   |    no    |
| municipality | string  |   no   |    no    |
| _state_id_   |   obj   |  yes   |    no    |


## State :
***

| NAME   |  TYPE  | UNIQUE | OPTIONAL |
| ------ | :----: | :----: | :------: |
| name   | string |  yes   |    no    |
| abbrev | string |  yes   |    no    |

## Message:
***
| NAME     |   TYPE   | UNIQUE | OPTIONAL |
| -------- | :------: | :----: | :------: |
| id       |    id    |  yes   |    no    |
| sender   |  email   |   no   |    no    |
| receiver |  email   |   no   |    no    |
| sent_at  | datetime |   no   |    no    |
| message  |  string  |   no   |    no    |