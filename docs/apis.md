# Categories
* **Method**: `POST`
* **Path**: /api/categories

Input:
```json
{
  "name": string
}
```

output:
```json
{
  "id": int
  "name": string
}
```
Creating a new category uses the incoming name of a category. It checks to see if the category already exists in the database. If the category doesn't exist, it saves the name to a new row of the database. It returns the new category with the new database id. This is only accessible by site admin.

* **Method**: `GET`
* **Path**: /api/categories

Output:
```json
{
  "id": int
  "name": string
}
```
Method to retrieve all categories.


# Brands
* **Method**: `POST`
* **Path**: /api/brands
Input:
```json
{
  "name": string
}
```

Output:
```json
{
  "id": int,
  "name": string
}
```
Creating a new brand uses the incoming name of a brand. It checks to see if the brand already exists in the database. If the brand doesn't exist, it saves the name to a new row of the database. It returns the new brand with the new database id. This is only accessable by site admin.

* **Method**: `GET`
* **Path**: /api/brands
Output:
```json
"brands": [
  {
  "id": int,
  "name": string
},
{
  "id": int,
  "name": string
}
]
```

Retrieves a list of dictionaries containing information on all brands including the name and id of each brand.

# Items for sale

* **Method**: `GET`
* **Path**: /api/inventory/for_sale
Output:
```json
"inventory" : [
  {
    "id": int,
    "seller": email,
    "title": str,
    "created_at": datetime,
    "category": {
      "id": int,
      "name": str
    },
    "condition": str,
    "brand": {
      "id": int,
      "name": str
    },
    "color": str,
    "size": str,
    "msrp": float,
    "description": str,
    "price": float,
    "sold": boolean,
    "post_status": string
  }
]
```
Retrieves a list of dictionaries containing information on all items for sale in the inventory.

* **Method**: `POST`
* **Path**: /api/inventory
Input:
```json
  {
    "title": str,
    "created_at": datetime=Auto Generated,
    "category": int,
    "condition": str,
    "brand": int,
    "color": str,
    "size": str,
    "msrp": float,
    "description": str,
    "price": float,
    "sold": boolean=False,
    "post_status": string=PENDING,
    "inappropriate_content": int=0
  }
```

Output:
```json
{
    "id": int,
    "title": str,
    "created_at": datetime,
    "category": {
      "id": int,
      "name": str
    },
    "condition": str,
    "brand": {
      "id": int,
      "name": str
    },
    "color": str,
    "size": str,
    "msrp": float,
    "description": str,
    "price": float,
    "sold": boolean,
    "post_status": string,
    "inappropriate_content": int=0
  }
```
Allows users to post an item for sale in the database. The post_status will be marked 'pending' by default and a human will review all posts and watch for inapproriate content.

* **Method**: `GET`
* **Path**: /api/inventory/pending

Output:
```json
{
    "id": int,
    "title": str,
    "created_at": datetime,
    "category": {
      "id": int,
      "name": str
    },
    "condition": str,
    "brand": {
      "id": int,
      "name": str
    },
    "color": str,
    "size": str,
    "msrp": float,
    "description": str,
    "price": float,
    "sold": boolean,
    "post_status": string,
    "inappropriate_content": int=0
  }
```
Retrieves all inventory items that have a post_status of 'pending'. Site administrators can approve or deny posts to allow them onto the site. This way we can screen for inappropriate content.

* **Method**: `GET`
* **Path**: /api/inventory/inappropriate

Output:
```json
{
    "id": int,
    "title": str,
    "created_at": datetime,
    "category": {
      "id": int,
      "name": str
    },
    "condition": str,
    "brand": {
      "id": int,
      "name": str
    },
    "color": str,
    "size": str,
    "msrp": float,
    "description": str,
    "price": float,
    "sold": boolean,
    "post_status": string,
    "inappropriate_content": int=0
  }
```
Users can report inappropriate content. Retrieves all inventory items that have an inappropriate_content status of >0. If users report the content as inappropriate, the integer at 'inappropriate_content' will increment by one. Site administrators can view all inventory items that users mark as inappropriate. A secondary method of screening for inappropriate content. After reviewing, site admin can decide if the item will remain in inventory or not.

# Offer
* **Method**: `GET`
* **Path**: /api/offers/
* **Path**: /api/offers/{ offer_id }
* **Path**: /api/offers/filter/by_seller/{ email }/
* **Path**: /api/offers/filter/by_buyer/{ email }/

Output:
```json
{
  "id": int,
  "seller": email,
  "buyer": email,
  "item": int,
  "created_at": datetime,
  "accepted_at": datetime,
  "price": float,
  "in_person": boolean
}
```
We can query all offers, a single offer by id, or all offers filtered by either buyer or seller. the latter two enpoints are used on the users dashboard page.

* **Method**: `POST`
* **Path**: /api/offers/
* **Method**: `PUT`
* **Path**: /api/offers/{ offer_id }

Input:
```json
{
  "id": int,
  "seller": email,
  "buyer": email,
  "item": int,
  "accepted_at": datetime,
  "price": float,
  "in_person": boolean
}
```
When a buyer makes a request to purchase an item, an offer object is created _without_ assigning an "accepted_at" value. when the seller accepts the offer, the object is then updated with the "accepted_at" value by declaring the offer_id in the endpoint and assigning the current time to "accepted_at". the "item" property takes an integer value that should correspond to the item_id. This needs to be provided by the front-end on object creation.

- How to mark something as sold?
  - send a PUT request to the endpoint /offers/{ offer_id }
- After the buyer accepts the offer?
- What if the deal falls through?
- After the buyer accepts the offer AND marks "sold" on their posting management?
  - the seller can cancel/reject the offer
  - the buyer can cancel/reject the offer


# Messages
* **Method**: `POST`
* **Path**: /api/messages/{user}

Input:
```json
{
  "sender": email,
  "receiver": email,
  "sent_at": datetime,
  "message": string
}
```
# Rating
* **Method**: `GET`
* **Path**: /api/users/rating/{ user_id }/

output:
```json
{
  "seller_id": int,
  "rating": {
    "number__avg": float
  }
}
```
sending a request for the User rating returns an object containing the user id and average rating for that user
***
* **Method**: `POST`
* **Path**: /api/users/rating/

input:
```json
{
  "seller_id": int,
  "buyer": email,
  "number": 1 <= int >= 5 
}
```

