import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "query_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    print("Inside process approval")
    data = json.loads(body)
    print(data)
    send_mail(
        "Your posting with Isella has been accepted!",
        f'Hello! We are happy to let you that your item {data["item_title"]} has been accepted and has been posted for sale.',
        "admin@isella.com",
        [data["seller_email"]],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    print("Inside process rejection")
    data = json.loads(body)
    print(data)
    send_mail(
        "Your posting with Isella has not been accepted.",
        f'Hello! Unfortunately, we were unable to list your item {data["item_title"]} for sale. This is likely due to the content not being in alignment with our mission.',
        "admin@isella.com",
        [data["seller_email"]],
        fail_silently=False,
    )


def process_query_match(ch, method, properties, body):
    data = json.loads(body)
    send_mail(
        f'Your query "{data["query"]}" returned a match at Isella!',
        f'Hello! You signed up to receive a notification based on the search: "{data["query"]}". Yay! Something matching that description has been posted. The match title is: {data["matching_item"]}',
        "admin@isella.com",
        [data["user_email"]],
        fail_silently=False,
    )


def process_received_offer(ch, method, properties, body):
    print("  Received %r" % body)
    data = json.loads(body)
    send_mail(
        f'Your received an offer on your item: "{data["item_title"]}"!',
        f'Hello! You received an offer on your item: "{data["item_title"]}" for {data["offer_price"]}. Yay! Log into Isella to check out your offer. You can accept the offer, or wait for others!',
        "admin@isella.com",
        [data["seller"]],
        fail_silently=False,
    )


def process_accepted_offer(ch, method, properties, body):
    print("  Received %r" % body)
    data = json.loads(body)
    send_mail(
        f'You accepted an offer for {data["item_title"]}!',
        f'Hello! You accepted an offer for your item titled "{data["item_title"]}". The buyer email address is: {data["buyer_email"]}. Please contact them to arrange the sale.',
        "admin@isella.com",
        [data["seller_email"]],
        fail_silently=False,
    )
    send_mail(
        f'Your offer has been accepted for {data["item_title"]}!',
        f'Hello! Your offer has been accepted for the item titled "{data["item_title"]}". The seller email address is: {data["seller_email"]}. Please contact them to arrange the sale.',
        "admin@isella.com",
        [data["buyer_email"]],
        fail_silently=False,
    )


while True:
    try:

        def main():
            # parameters = pika.URLParameters('amqp://guest:guest@rabbit-server1:5672/%2F')
            parameters = pika.URLParameters(os.environ["CLOUDAMQP_URL"])
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()

            # CODE FOR QUEUES
            channel.queue_declare(queue="process_approval")
            channel.basic_consume(
                queue="process_approval",
                on_message_callback=process_approval,
                auto_ack=True,
            )

            channel.queue_declare(queue="process_rejection")
            channel.basic_consume(
                queue="process_rejection",
                on_message_callback=process_rejection,
                auto_ack=True,
            )

            channel.queue_declare(queue="process_query_match")
            channel.basic_consume(
                queue="process_query_match",
                on_message_callback=process_query_match,
                auto_ack=True,
            )

            channel.queue_declare(queue="process_accepted_offer")
            channel.basic_consume(
                queue="process_accepted_offer",
                on_message_callback=process_accepted_offer,
                auto_ack=True,
            )

            channel.queue_declare(queue="process_offer_received")
            channel.basic_consume(
                queue="process_offer_received",
                on_message_callback=process_received_offer,
                auto_ack=True,
            )

            print(" [*] Waiting for messages. To exit press CTRL+C")
            channel.start_consuming()

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError as e:
        print("Could not connect to RabbitMQ")
        print(e)
        time.sleep(2.0)
