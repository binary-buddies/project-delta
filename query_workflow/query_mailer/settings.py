import requests
import os

DEBUG = os.environ.get("DEBUG", False) == "True"
ALLOWED_HOSTS = []
LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True

EMAIL_HOST = "mail"
EMAIL_SUBJECT_PREFIX = ""

if not DEBUG:
    response = requests.get(
        "https://mailtrap.io/api/v1/inboxes.json?api_token=<MAILTRAP_API_TOKEN>"
    )
    credentials = response.json()[0]

    EMAIL_HOST = credentials["domain"]
    EMAIL_HOST_USER = credentials["username"]
    EMAIL_HOST_PASSWORD = credentials["password"]
    EMAIL_PORT = credentials["smtp_ports"][0]
    EMAIL_USE_TLS = True
