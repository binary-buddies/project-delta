from fastapi import FastAPI
import os
from fastapi.middleware.cors import CORSMiddleware
import requests
from keys import OPEN_WEATHER_API_KEY
import json

app = FastAPI()

origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/weather")
def get_weather_data(city: str, state: str):

    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ",US&limit=5&appid="
        + OPEN_WEATHER_API_KEY
    )

    response = requests.get(url)

    log_lat_data = json.loads(response.content)[0]

    lon = log_lat_data["lon"]
    lat = log_lat_data["lat"]

    newUrl = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY }&unit=imperial"
    response = requests.get(newUrl)
    return response.json()
