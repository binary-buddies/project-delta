#!/bin/bash
docker compose down
docker volume remove beta-data
docker volume create beta-data
docker compose build
docker compose up