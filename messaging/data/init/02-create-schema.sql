\connect messaging_app_db

---
--- Create the tables in your database
---
CREATE TABLE messages (
    id SERIAL NOT NULL PRIMARY KEY,
    sender VARCHAR(250),
    receiver VARCHAR(250),
    -- sent_at timestamp,
    content VARCHAR(250)
);

---
--- Change the owner from postgres superuser to the user for
--- the database
---
ALTER TABLE messages OWNER TO messaging_app_user;