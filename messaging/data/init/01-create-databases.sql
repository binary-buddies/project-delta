-- In here, you can create any extra users and databases
-- that you might need for all of your services

CREATE USER messaging_app_user WITH LOGIN PASSWORD 'secret';

CREATE DATABASE messaging_app_db WITH OWNER messaging_app_user;