from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os

import chat as chat

from routers import (
    messages as messages,
)

app = FastAPI()

origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# PostgreSQL endpoints
app.include_router(messages.router)

# Messaging App
app.include_router(chat.router)
