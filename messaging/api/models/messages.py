from pydantic import BaseModel


class MessageOut(BaseModel):
    id: int
    sender: str
    receiver: str
    content: str


class MessageIn(BaseModel):
    sender: str
    receiver: str
    content: str


class MessageList(BaseModel):
    messages: list[MessageOut]
