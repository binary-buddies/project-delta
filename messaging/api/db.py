from psycopg_pool import ConnectionPool

pool = ConnectionPool()


class MessageQueries:
    def get_all_messages(self):
        with pool.connection() as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    """
          SELECT m.id
                , m.sender
                , m.receiver
                , m.content
          FROM messages m
          """
                )
                rows = cursor.fetchall()
                return list(rows)

    def insert_message(self, content, sender, receiver):
        with pool.connection() as connection:
            with connection.cursor() as cursor:
                try:
                    cursor.execute(
                        """
            INSERT INTO messages(sender, receiver, content)
            VALUES (%s, %s, %s)
            RETURNING id, sender, receiver, content
            """,
                        [sender, receiver, content],
                    )
                    return cursor.fetchone()
                except Exception:
                    return "Error"
