from fastapi import APIRouter, Depends, Response
from models.messages import MessageIn, MessageOut, MessageList
from db import MessageQueries

router = APIRouter()


def row_to_message(row):
    message = {
        "id": row[0],
        "sender": row[1],
        "receiver": row[3],
        "content": row[2],
    }
    return message


@router.get(
    "/api/messages", response_model=MessageList, responses={200: {"model": MessageList}}
)
def get_messages(query=Depends(MessageQueries)):
    messages = query.get_all_messages()
    print(messages)
    return {
        "messages": [row_to_message(message) for message in messages],
    }


@router.post(
    "/api/messages", response_model=MessageOut, responses={200: {"model": MessageOut}}
)
def create_message(
    message: MessageIn, response: Response, query=Depends(MessageQueries)
):
    try:
        row = query.insert_message(message.sender, message.receiver, message.content)
        return row_to_message(row)
    except Exception:
        return {"message": "Message cannot be posted"}
